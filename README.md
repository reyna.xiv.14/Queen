# Queen
Queen of the Seas Game

[IGDB Entry](https://tfgames.site/index.php?module=viewgame&id=1680)

[Discussion Forum](https://tfgames.site/phpbb3/viewtopic.php?f=6&t=11112)

## Downloads

You can download compiled files and source archives from the [Releases](https://gitlab.com/queen-of-the-seas/Queen/-/releases) page, and the [build](https://gitlab.com/queen-of-the-seas/Queen/-/jobs/artifacts/master/download?job=build) from the latest commit to the master branch.

This game uses the Twine engine with the Sugarcube 2.x format.

## How to build the game

1. Download sources either for a latest release from the [release page](https://gitlab.com/queen-of-the-seas/Queen/-/releases), [snapshot of the master branch](https://gitlab.com/queen-of-the-seas/Queen/-/tree/master) or clone the Git repository.
2. Download and install Tweego and the associated story formats from https://www.motoslave.net/tweego/
3. Download the latest version of the SugarCube2 story format from [its GitHub page](https://github.com/tmedwards/sugarcube-2/releases). Choose the "local" and highest twine version variant. Overwrite storyformat files from the Tweego distribution with these ones.
4. Install [Node.js](https://nodejs.org/).
5. Set the [TWEEGO_PATH](https://www.motoslave.net/tweego/docs/#getting-started-environment-variables) environment variable to include the directory where you installed your story formats.
6. Run `npm install` in the sources directory (directory where you unpacked the sources archive or cloned git repository into) to install required dependencies.
7. Run `npx gulp` in the source directory. (Make sure tweego executable is in your path). The compilation result is located at `build/Queen.html`.

To install required software on Windows, you can use the winget tool:
```
winget install Git.Git
winget install OpenJS.NodeJS
```
If you opt in to choose install options for these packages (i.e. run `winget install -i`), ensure that both packages still append their binaries to the `PATH` environment variable (this is enabled by default).

## How to modify the game

Even if you are not a javascript programmer, so long as you can understand the JSON data format and open a text editor you should be able to modify this game. All data related files are stored in the [001-SCRIPT_DATA](src/001-SCRIPT_DATA) directory.

Adding rooms and such follows the normal twine paradigm. There are a few useful widgets that I have created in the [src/200-WIDGETS](src/200-WIDGETS) directory that will help.

### Adding NPCs and stores

This is slightly more tricky. Once you have edited the data files to add an NPC and store there are a couple of things you need to do.

1 - Go to the room you want the npc to appear in and use the widget <<NPC "SomeNpcName">> replacing the "SomeNpcName" part with the name you assigned your NPC in the data file as their key.

2 - Edit player.js and add the NPC to the NPC structure (look for this.NPCS). There are plenty of examples there on how to do it.

3 - Adding a store: You simply need to add another line to the this.StoreInventory data structure. Also this is fairly self explanatory if you look at it.

Edit these files, build the game and then launch it in your browser.

Happy Pillaging!
