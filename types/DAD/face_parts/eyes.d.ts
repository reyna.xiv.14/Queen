import {FacePart} from "./face_part";
import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";

declare class Eyes extends FacePart {
	constructor(...data: object[]);
}


export class EyesHuman extends Eyes {
	constructor(...data: object[]);

	fill(): string;
    calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
