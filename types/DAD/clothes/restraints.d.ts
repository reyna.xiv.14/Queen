import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";

export class RestraintChainPart extends ClothingPart {
	constructor(...data: object[]);

	chainWidth: number;
	chainStroke: string;
	chainDash: number[];
}

export class BondageRopePart extends RestraintChainPart {
	constructor(...data: object[]);
}

export class ChestBondagePart extends BondageRopePart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class WaistBondagePart extends BondageRopePart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class ChestPentagramBondagePart extends BondageRopePart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class WristRestraintChainPart extends RestraintChainPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class AnkleRestraintChainPart extends RestraintChainPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

/**
 * Base Clothing classes
 */
export class Restraints extends Clothing {
	constructor(...data: object[]);
}

export class Bondage extends Clothing {
}

/**
 * Concrete Clothing classes
 */
export class WristRestraints extends Restraints {
	constructor(...data: object[]);
	Mods: {
		armRotation: number;
		handRotation: number;
	}

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class AnkleRestraints extends Restraints {
	constructor(...data: object[]);

	startAlongLeg: number;
	legCoverage: number;

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class ChestBondage extends Bondage {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class ChestPentagramBondage extends Bondage {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class WaistBondage extends Bondage {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
