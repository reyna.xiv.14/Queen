Object.defineProperty(Array.prototype, 'randomElement', {
	configurable: true,
	writable: true,

	value(this: unknown[]) {
		if (this.length === 0) {
			throw "randomElement: Array is empty";
		}

		return this.random();
	},
});

Object.defineProperty(Array.prototype, 'retrieve', {
	configurable: true,
	writable: false,

	value(this: unknown[], predicate: (value: unknown, index: number, obj: unknown[]) => unknown) {
		const res = this.find(predicate);
		if (res === undefined) {
			throw new Error("Could not find satisfying value in the array")
		}
		return res;
	},
});

Object.defineProperty(Object.prototype, 'append', {
	value(target: Record<PropertyKey, unknown>, source: Record<PropertyKey, unknown>): Record<PropertyKey, unknown> {
		for (const p in source) {
			if (target.hasOwnProperty(p)) {
				throw new Error(`Property ${p} already defined`);
			}
			target[p] = source[p];
		}
		return target;
	},
});

Object.defineProperty(Object.prototype, 'hasKey', {
	value(k: PropertyKey): boolean {
		return k in this;
	},
});

Object.defineProperty(Math, 'mean', {
	configurable: true,
	writable: true,

	value(v0: number, ...vn: number[]): number {
		let sum = v0;
		for (const v of vn) {
			sum += v;
		}
		return sum / (vn.length + 1);
	},
});

Object.defineProperty(_, 'pascalCase', {
	value(this: typeof _, s: string): string {
		return s.length > 0 ? s[0].toUpperCase() + this.camelCase(s.slice(1)) : s;
	},
});
