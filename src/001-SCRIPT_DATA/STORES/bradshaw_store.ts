namespace App.Data {
	stores.bradshaw = {
		name: "One-Eyed Bradshaw", open: [0, 1, 2, 3], restock: 7,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "cheap wine"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "rum"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "expensive wine"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "ol musky"},
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "pirates plunder"},
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "ambrosia"},
		],
	};
}
