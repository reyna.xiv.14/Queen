namespace App.Data {
	stores.cargo = {
		name: "Julius's Secret Stash",
		open: [2, 3],
		restock: 7,
		maxRares: 2,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "pixie dust"}, // Energy
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 4, max: 4, price: 1.0, mood: 20, lock: 0, tag: "medicinal herbs"}, // Healing
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 4, max: 4, price: 1.0, mood: 40, lock: 0, tag: "fairy dust"}, // Energy
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 2, max: 2, price: 1.0, mood: 80, lock: 0, tag: "face cream"}, // Face
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 2, max: 2, price: 1.0, mood: 70, lock: 0, tag: "lip balm"}, // Lips
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 2, max: 2, price: 1.0, mood: 20, lock: 1, tag: "khafkir"}, // Heal + Male
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 60, lock: 1, tag: "siren elixir"}, // Lots of female bod, +Will
			// RARE ITEMS DRUGS & CONSUMABLES
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 2, max: 2, price: 1.0, mood: 60, lock: 0, tag: "hair tonic"}, // Hair
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 80, lock: 0, tag: "nereid philtre"}, // unique
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 80, lock: 0, tag: "succubus philtre"}, // unique
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "brown hair dye"},
			// RARE REELS
			{category: Items.Rarity.Rare, type: Items.Category.Reel, qty: 1, max: 1, price: 1.0, mood: 70, lock: 0, tag: "commonWhore"},
			{category: Items.Rarity.Rare, type: Items.Category.Reel, qty: 1, max: 1, price: 1.0, mood: 80, lock: 0, tag: "commonWildcard"},
			{category: Items.Rarity.Rare, type: Items.Category.Reel, qty: 1, max: 1, price: 1.0, mood: 90, lock: 0, tag: "uncommonWhore"},
			// RARE APOTHECARY
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 70, lock: 0, tag: "apDaisy"}, // fitness and heal
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 70, lock: 0, tag: "apBaneberry"}, // boobs
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 70, lock: 0, tag: "apTanzy"}, // dick and balls
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 70, lock: 0, tag: "apThorn"}, // boobs + face
		],
	};
}
