namespace App.Data {
	Object.append(whoring, {
		SaltyMermaid: {
			// Shows in the UI as location.
			desc: "The Salty Mermaid",
			// Sex act weighted table.
			wants: {hand: 3, ass: 4, bj: 4, tits: 2},
			// Minimum payout rank.
			minPay: 1,
			// Maximum payout rank.
			maxPay: 2,
			// Bonus payout, weighted table
			bonus: {
				body: {bust: 4, ass: 3, face: 2, lips: 1},
				style: {Bimbo: 1, SexyDancer: 1, PirateSlut: 3},
				stat: {[CoreStat.Perversion]: 1, [CoreStat.Femininity]: 2, beauty: 1},
			},
			names: Data.names.Male,
			title: "Pirate",
			rares: ["Reginald", "Kipler", "Cookie", "Julius"],
			npcTag: "Crew",
			marquee: "mq_salty_mermaid",
			available: [{type: 'quest', name: 'Q001', property: 'status', value: QuestStatus.Completed}],
		},

		Reginald: {
			// Shows in the UI as location.
			desc: "The Salty Mermaid",
			// Sex act weighted table.
			wants: {ass: 1, bj: 1},
			// Minimum payout rank.
			minPay: 3,
			// Maximum payout rank.
			maxPay: 5,
			// Bonus payout, weighted table
			bonus: {
				body: {bust: 4, ass: 3, face: 2, lips: 1},
				style: {Bdsm: 2, PirateSlut: 2},
				stat: {[CoreStat.Perversion]: 3, [CoreStat.Femininity]: 2, beauty: 1},
			},
			names: ["Reginald"],
			title: "Captain",
			rares: [],
			npcTag: "Captain",
		},

		Kipler: {
			// Shows in the UI as location.
			desc: "The Salty Mermaid",
			// Sex act weighted table.
			wants: {tits: 2, ass: 1, bj: 1},
			// Minimum payout rank.
			minPay: 3,
			// Maximum payout rank.
			maxPay: 4,
			// Bonus payout, weighted table
			bonus: {
				body: {bust: 5, ass: 3},
				style: {PetGirl: 2, PirateSlut: 2},
				stat: {[CoreStat.Perversion]: 3, [CoreStat.Femininity]: 2, beauty: 1},
			},
			names: ["Kipler"],
			title: "1st Mate",
			rares: [],
			npcTag: "FirstMate",
		},

		Cookie: {
			// Shows in the UI as location.
			desc: "The Salty Mermaid",
			// Sex act weighted table.
			wants: {ass: 3, bj: 1, tits: 2, hand: 1},
			// Minimum payout rank.
			minPay: 3,
			// Maximum payout rank.
			maxPay: 3,
			// Bonus payout, weighted table
			bonus: {
				body: {bust: 4, ass: 3, face: 2, lips: 1},
				style: {DaddyGirl: 2, SissyLolita: 2},
				stat: {[CoreStat.Perversion]: 3, [CoreStat.Femininity]: 2, beauty: 1},
			},
			names: ["Cookie"],
			title: "Chef",
			rares: [],
			npcTag: "Cook",
		},

		Julius: {
			// Shows in the UI as location.
			desc: "The Salty Mermaid",
			// Sex act weighted table.
			wants: {ass: 3, bj: 1, tits: 2, hand: 1},
			// Minimum payout rank.
			minPay: 3,
			// Maximum payout rank.
			maxPay: 3,
			// Bonus payout, weighted table
			bonus: {
				body: {bust: 2, ass: 6, face: 1, lips: 1},
				style: {Bimbo: 1, DaddyGirl: 1, PirateSlut: 1},
				stat: {[CoreStat.Perversion]: 3, [CoreStat.Femininity]: 2, beauty: 1},
			},
			names: ["Julius"],
			title: "Qtr. Mstr.",
			rares: [],
			npcTag: "Quartermaster",
		},
	});
}
