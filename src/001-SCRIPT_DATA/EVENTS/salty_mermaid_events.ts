// Template for making events - the key is the destination passage you are traveling to.

namespace App.Data {
	Object.append(events, {
		Cabin: [
			{
				id: 'CrewRape', // A unique ID for the event.
				from: 'Sleep', // The passage you are traversing from
				maxRepeat: 0, // Number of times the event can repeat, 0 means forever.
				minDay: 5, // Minimum day the event shows up on
				maxDay: 0, // Maximum day the event shows up on, 0 means forever
				cool: 3, // Interval between repeats on this event.
				phase: [0], // Time phases the event is valid for
				passage: 'CrewRapeEvent', // Override passage that the player is routed to.
				check: () => (setup.world.npc(Entity.NpcId.Crew).mood < 40) && State.random() < 0.25,
			},
			{
				id: 'PenisShrink',
				from: 'Sleep',
				maxRepeat: 1,
				minDay: 1,
				maxDay: 0,
				cool: 0,
				phase: [0],
				passage: 'PenisShrinkEvent',
				check: p => (p.stat(Stat.Body, BodyPart.Penis) <= 34 && p.stat(Stat.Body, BodyPart.Balls) <= 34),
			},
			{
				id: 'PenisChastityCageFeltDown',
				from: 'Sleep',
				maxRepeat: 1,
				minDay: 1,
				maxDay: 0,
				cool: 0,
				phase: [0],
				passage: 'ChastityCageFallDownEvent',
				check: (p) => {
					if (p.stat(Stat.Body, BodyPart.Penis) > 9 || p.stat(Stat.Body, BodyPart.Balls) > 9) {
						return false;
					}
					const eq = p.equipmentInSlot(ClothingSlot.Penis);
					return (eq?.isLocked === true && eq.name.includes("chastity cage"));
				},
			},
			{
				id: 'KrakenAttack',
				from: 'Sleep',
				maxRepeat: 0,
				minDay: 20,
				maxDay: 0,
				cool: 10,
				phase: [0],
				passage: 'KrakenAttackEvent',
				check: () => !setup.world.pc.isInPort() && State.random() < 0.2,
			},
			{
				id: 'PirateAttack',
				from: 'Sleep',
				maxRepeat: 0,
				minDay: 25,
				maxDay: 0,
				cool: 15,
				phase: [0],
				passage: 'PirateAttackEvent',
				check: () => !setup.world.pc.isInPort() && State.random() < 0.2,
			},
			{
				id: 'StarvationEvent',
				from: 'Sleep',
				force: true,
				maxRepeat: 1,
				minDay: 1,
				maxDay: 30,
				cool: 0,
				phase: [0],
				passage: 'StarvationEvent',
				check: p => p.stat(Stat.Core, CoreStat.Nutrition) <= 20,
			},
			{
				id: 'ToxicityEvent',
				from: 'Sleep',
				force: true,
				maxRepeat: 1,
				minDay: 1,
				maxDay: 30,
				cool: 0,
				phase: [0],
				passage: 'ToxicityEvent',
				check: p => p.stat(Stat.Core, CoreStat.Toxicity) >= 100,
			},
		],

		Deck: [
			{
				id: 'CoffinDiceIntroduction',
				from: 'Any',
				maxRepeat: 1,
				minDay: 20,
				maxDay: 0,
				cool: 0,
				phase: [2, 3],
				passage: 'CoffinDiceEvent',
				check: (p) => {
					return (
						((p.stat(Stat.Skill, Skills.Sexual.HandJobs) >= 20) ||
							(p.stat(Stat.Skill, Skills.Sexual.BlowJobs) >= 20) ||
							(p.stat(Stat.Skill, Skills.Sexual.TitFucking) >= 20) ||
							(p.stat(Stat.Skill, Skills.Sexual.AssFucking) >= 20)) &&
						(State.random() < 0.25));
				},
			},
			{
				id: 'FightClubIntroduction',
				from: 'Any',
				maxRepeat: 1,
				minDay: 20,
				maxDay: 0,
				cool: 0,
				phase: [2, 3],
				passage: 'FightClubEvent',
				check: () => State.random() < 0.25,
			},
			{
				id: 'SirenAttack',
				from: 'Any',
				maxRepeat: 0,
				minDay: 40,
				maxDay: 0,
				cool: 20,
				phase: [3],
				passage: 'SirenAttackEvent',
				check: () => !setup.world.pc.isInPort() && State.random() < 0.2,
			},
			{
				id: 'KiplerSpar',
				from: 'Any',
				maxRepeat: 1,
				minDay: 10,
				maxDay: 0,
				cool: 3,
				phase: [2],
				passage: 'KiplerSparEvent',
				check: p => p.stat(Stat.Skill, Skills.Piracy.Swashbuckling) >= 20 && (State.random() < 0.25),
			},
		],
	});
}
