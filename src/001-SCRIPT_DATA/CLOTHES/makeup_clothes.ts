namespace App.Data {
	Object.append(App.Data.clothes, {
		"black mascara": { // +12
			name: "black mascara", shortDesc: "black mascara",
			longDesc: "black mascara",
			slot: ClothingSlot.Mascara,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: [],
			style: [],
			inMarket: false,
		},

		"red mascara": { // +12
			name: "red mascara", shortDesc: "red mascara",
			longDesc: "black mascara",
			slot: ClothingSlot.Mascara,
			color: "red", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: [],
			style: [],
			inMarket: false,
		},

		"green mascara": { // +12
			name: "green mascara", shortDesc: "green mascara",
			longDesc: "green mascara",
			slot: ClothingSlot.Mascara,
			color: "green", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: [],
			style: [],
			inMarket: false,
		},

		"yellow mascara": { // +12
			name: "yellow mascara", shortDesc: "yellow mascara",
			longDesc: "yellow mascara",
			slot: ClothingSlot.Mascara,
			color: "yellow", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: [],
			style: [],
			inMarket: false,
		},
	});
}
