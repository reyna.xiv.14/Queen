// STYLE TABLE
// TYPE         COMMON  UNCOMMON    RARE    LEGENDARY
// ACCESSORY    3       6           9       12
// CLOTHING     5       10          15      20
// ONE PIECE    10      20          30      40
namespace App.Data {
	Object.append(App.Data.clothes, {
		// WIG SLOT
		"pigtail wig": { // +9
			name: "pigtail wig", shortDesc: "a {COLOR} wig with pigtails and accessories",
			longDesc: "This high quality wig is done up in pigtails fashioned with little heart accessories that have 'I @@.state-girlinness,&#9825,@@ Daddy!' written on them.",
			slot: ClothingSlot.Wig, color: "blond", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			hairLength: 60, hairStyle: Fashion.HairStyle.PigTails, hairBonus: 60,
			style: [Fashion.Style.DaddyGirl], wearEffect: ["FEMININE_CLOTHING"],
		},

		// HAT SLOT

		// NECK SLOT
		"collar for daddy": { // +9
			name: "collar for daddy", shortDesc: "a lacey {COLOR} collar with pink hearts",
			longDesc: "This lacey white collar is fashioned with little pink hearts that combine to form the words '@@.state-girlinness,Daddy's Girl@@!' as a whole.",
			slot: ClothingSlot.Neck, color: "white", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Accessory,
			style: [Fashion.Style.DaddyGirl], wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"],
		},

		// NIPPLES SLOT

		// BRA SLOT
		"cute pink bra": { // +10
			name: "cute pink bra", shortDesc: "a cute {COLOR} bra with tiny hearts on it",
			longDesc: "This cute bra has tiny white &#9825, on it and a delicate little bow in the middle.",
			slot: ClothingSlot.Bra, color: "pink", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.DaddyGirl],
		},

		// CORSET SLOT
		"belt with glitter": { // +9
			name: "belt with glitter", shortDesc: "a {COLOR} belt with glittery letters",
			longDesc: "This white belt features pink glitter spelling out the word '@@.state-girlinness,PRECIOUS@@'.",
			slot: ClothingSlot.Corset, color: "white", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING", "WAIST_CINCHING"], style: [Fashion.Style.DaddyGirl, Fashion.Style.Bimbo, Fashion.Style.PetGirl],
		},

		// PANTY SLOT
		"cute pink panties": { // +15
			name: "cute pink panties", shortDesc: "a pair of cute {COLOR} panties",
			longDesc: "On the surface these cute pink panties look quite demure, but they have 'I @@.state-girlinness,&#9825,@@ Daddy!' written across the backside.",
			slot: ClothingSlot.Panty, color: "pink", rarity: Items.Rarity.Rare, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.DaddyGirl],
		},

		// STOCKINGS SLOT
		"pink knee socks": { // +10
			name: "pink knee socks", shortDesc: "a pair of {COLOR} knee socks",
			longDesc: "These pink socks come up right to your knee and have little ties to hold them up.",
			slot: ClothingSlot.Stockings, color: "pink", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.DaddyGirl, Fashion.Style.Ordinary, Fashion.Style.Bimbo],
		},

		"white knee socks": { // +10
			name: "white knee socks", shortDesc: "a pair of {COLOR} knee socks",
			longDesc: "These white socks come up right to your knee and have little ties to hold them up.",
			slot: ClothingSlot.Stockings, color: "white", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.DaddyGirl],
		},

		// SHIRT SLOT
		"pink t-shirt": { // +20
			name: "pink t-shirt",
			shortDesc: "a naughty {COLOR} t-shirt",
			longDesc: "\
    This t-shirt is cut short, designed to show off a lot of under-boob. It says \
    '@@.state-girlinness,&#9825,@@Daddy's Little Cumslut'@@.state-girlinness,&#9825,@@ on it.",
			slot: ClothingSlot.Shirt,
			restrict: [ClothingSlot.Shirt, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "pink",
			rarity: Items.Rarity.Legendary,
			type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"],
			style: [Fashion.Style.DaddyGirl],
		},

		// PANTS SLOT
		"pink tartan": { // +20
			name: "pink tartan",
			shortDesc: "a very short {COLOR} tartan schoolgirl's skirt",
			longDesc: "\
    This pleated skirt is pink with a black and deep purple tartan pattern. It's incredibly short. Way too short.",
			slot: ClothingSlot.Pants,
			restrict: [ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "pink",
			rarity: Items.Rarity.Legendary,
			type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"],
			style: [Fashion.Style.DaddyGirl, Fashion.Style.SexyDancer, Fashion.Style.Bimbo],
		},

		// DRESS SLOT
		"pink gingham dress": { // +40
			name: "pink gingham dress", shortDesc: "a super short {COLOR} gingham dress",
			longDesc: "This dress has a girly gingham pattern with white lace collar trim. Normally it'd look quite demure, but it's so short it barely covers your arse and it has a very prominent cleavage window to show off your assets.",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "pink", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.DaddyGirl],
			inMarket: false,
		},

		"babydoll dress": { // +40
			name: "babydoll dress", shortDesc: "{COLOR} babydoll dress",
			longDesc: "\
    This humiliatingly short dress is made out of a light breezy material that is almost see-through. It has a plunging\
    neckline that leaves little to the imagination and delicate embroidery across the collar and hem depicting \
    an assortment of wildflowers.\
    ",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "white", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: ["FLIRTY"],
			style: [Fashion.Style.DaddyGirl],
			inMarket: false,
		},

		// COSTUME SLOT
		"naughty schoolgirl outfit": { // +40
			name: "naughty schoolgirl outfit", shortDesc: "naughty schoolgirl uniform",
			longDesc: "\
    This costume is designed to copy a typical schoolgirl uniform - white shirt with blazer and a tartan skirt, \
    however the skirt is far too short to be decent, the shirt is more of a low cut tank top and the jacket is \
    so tiny that it's more of an accessory than anything else. It'd be ridiculous for a grown woman to wear this, \
    which is kind of the point.\
    ",
			slot: ClothingSlot.Costume, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "tartan", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["KINKY_CLOTHING"],
			activeEffect: ["MAJOR_STRIPPERS_ALLURE"],
			style: [Fashion.Style.DaddyGirl, Fashion.Style.SexyDancer],
			inMarket: false,
		},

		// SHOES SLOT
		"low heel maryjanes": { // +20
			name: "low heel maryjanes", shortDesc: "pair of {COLOR} low heeled maryjanes",
			longDesc: "\
    This style of shoe is popular with young girls for their fashionable look and ease of wearing. The heel \
    is higher than typical for this style, but not so much as to impair ones walking. The craftsmanship and \
    quality of the shoe is apparent in how sturdy and lightweight it is. \
    ",
			slot: ClothingSlot.Shoes,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: ["FANCY_MOVES"],
			style: [Fashion.Style.DaddyGirl, Fashion.Style.SexyDancer],
			inMarket: false,
		},
		// BUTT SLOT

		// PENIS SLOT
		"pink cock ribbon": { // +12
			name: "pink cock ribbon", shortDesc: "delicate {COLOR} cock ribbon",
			longDesc: "\
    A tiny ribbon meant to be worn around the cock and tied into a bow. The ribbon itself has the phrase \
    'I @@.state-girlinness,&#9825,@@ Daddy!' embroidered on it.\
    ",
			slot: ClothingSlot.Penis,
			color: "pink", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory,
			wearEffect: ["KINKY_CLOTHING"],
			activeEffect: [],
			style: [Fashion.Style.DaddyGirl],
			inMarket: false,
		},
		// WEAPON SLOT (huh?)
	});
}
