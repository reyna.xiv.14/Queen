namespace App.Data {
	const stdSkillEffect: Effects.TemplateData = {
		[Items.Rarity.Common]: {points: 50, value: 100, suffixCount: 1},
		[Items.Rarity.Uncommon]: {points: 100, value: 200, suffixCount: 2},
		[Items.Rarity.Rare]: {points: 200, value: 400, suffixCount: 3},
		[Items.Rarity.Legendary]: {points: 400, value: 800, suffixCount: 4},
	};

	function makeSkillEffects(skill: Skills.Any): Record<string, EffectDesc> {
		return Effects.makeEffects((p, a) => p.adjustSkillXP(skill, a), {
			[`${skill.toUpperCase()}_xp`]: {
				knowledge: _.upperFirst(skill), knowledgeSuffix: '+',
				data: stdSkillEffect,
			},
		});
	}

	Object.append(effectLibrary, makeSkillEffects(Skills.Piracy.Sailing));
	Object.append(effectLibrary, makeSkillEffects(Skills.Piracy.Swashbuckling));
	Object.append(effectLibrary, makeSkillEffects(Skills.Piracy.Navigating));

	Object.append(effectLibrary, makeSkillEffects(Skills.Domestic.Cleaning));
	Object.append(effectLibrary, makeSkillEffects(Skills.Domestic.Cooking));
	Object.append(effectLibrary, makeSkillEffects(Skills.Domestic.Serving));

	Object.append(effectLibrary, makeSkillEffects(Skills.Charisma.Dancing));
	Object.append(effectLibrary, makeSkillEffects(Skills.Charisma.Singing));
	Object.append(effectLibrary, makeSkillEffects(Skills.Charisma.Styling));
	Object.append(effectLibrary, makeSkillEffects(Skills.Charisma.Seduction));

	Object.append(effectLibrary, makeSkillEffects(Skills.Sexual.AssFucking));
	Object.append(effectLibrary, makeSkillEffects(Skills.Sexual.BlowJobs));
	Object.append(effectLibrary, makeSkillEffects(Skills.Sexual.HandJobs));
	Object.append(effectLibrary, makeSkillEffects(Skills.Sexual.TitFucking));

	Object.append(effectLibrary, {
		/** RANDOM SKILL XP */
		RANDOM_SKILL_XP_LEGENDARY: {
			fun: (p) => {
				p.adjustSkillXP(Object.keys(p.skills).randomElement(), 400);
			},
			value: 800, knowledge: ["Random Skill XP++++"],
		},
	});
}
