namespace App.Data {
	Object.append(effectLibrary, {
		SLAVE_GRUEL: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Nutrition, 15);
				p.adjustCoreStatXP(CoreStat.Nutrition, 75);
				p.adjustCoreStatXP(CoreStat.Willpower, -25);
				p.adjustCoreStatXP(CoreStat.Perversion, 50);
				p.adjustCoreStatXP(CoreStat.Hormones, 50);
			},
			value: 30,
			knowledge: [
				"Nutrition Up++", "Satiation+++", "WillPower Down-",
				"Perversion Up+", "Female Hormones++",
			],
		},
		SNACK: {
			fun: (p) => {p.adjustCoreStat(CoreStat.Nutrition, 5); p.adjustCoreStatXP(CoreStat.Nutrition, 10);},
			value: 10,
			knowledge: ["Nutrition Up", "Satiation Up+"],
		},

		UNWHOLESOME_MEAL: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Toxicity, 10);
			},
			value: 0,
			knowledge: ["Unwholesome Meal-"],
		},

		LIGHT_WHOLESOME_MEAL: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Nutrition, 10);
				p.adjustCoreStatXP(CoreStat.Nutrition, 25);
				p.adjustCoreStat(CoreStat.Toxicity, -5);
			},
			value: 30,
			knowledge: ["Wholesome Meal+"],
		},
		WHOLESOME_MEAL: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Nutrition, 20);
				p.adjustCoreStatXP(CoreStat.Nutrition, 50);
				p.adjustCoreStat(CoreStat.Toxicity, -10);
			},
			value: 60,
			knowledge: ["Wholesome Meal++"],
		},
		LIGHT_ALCOHOL: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Nutrition, 5);
				p.adjustCoreStatXP(CoreStat.Nutrition, 15);
				p.adjustCoreStat(CoreStat.Toxicity, 5);
			},
			value: 20,
			knowledge: ["Nutrition Up", "Satiation Up", "Toxicity Up"],
		},
		HARD_ALCOHOL: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Nutrition, 5);
				p.adjustCoreStatXP(CoreStat.Nutrition, 10);
				p.adjustCoreStat(CoreStat.Toxicity, 30);
				p.adjustCoreStatXP(CoreStat.Willpower, 50);
			},
			value: 100,
			knowledge: ["Nutrition Up", "Satiation Up", "Toxicity Up--", "WillPower Up+"],
		},
	});
}
