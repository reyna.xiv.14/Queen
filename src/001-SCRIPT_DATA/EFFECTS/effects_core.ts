namespace App.Data {

	const templateXpPoints = {
		[Items.Rarity.Common]: {points: 1, suffixCount: 1},
		[Items.Rarity.Uncommon]: {points: 2, suffixCount: 2},
		[Items.Rarity.Rare]: {points: 4, suffixCount: 3},
		[Items.Rarity.Legendary]: {points: 8, suffixCount: 4},
	};

	/** ENERGY */
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustCoreStat(CoreStat.Energy, a), {
		energy: {
			knowledge: "Energy Up", knowledgeSuffix: '+',
			data: {
				[Items.Rarity.Common]: {points: 1, value: 20, suffixCount: 1},
				[Items.Rarity.Uncommon]: {points: 3, value: 50, suffixCount: 2},
				[Items.Rarity.Rare]: {points: 5, value: 100, suffixCount: 3},
				[Items.Rarity.Legendary]: {points: 8, value: 200, suffixCount: 4},
			},
		},
	}));

	/** TOXICITY */
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustCoreStat(CoreStat.Toxicity, a), {
		toxicity: {
			knowledge: "Toxicity Up", knowledgeSuffix: '-', valueFactor: 0,
			data: {
				[Items.Rarity.Common]: {points: 10, suffixCount: 1},
				[Items.Rarity.Uncommon]: {points: 30, suffixCount: 2},
				[Items.Rarity.Rare]: {points: 50, suffixCount: 3},
				[Items.Rarity.Legendary]: {points: 80, suffixCount: 4},
			},
		},
		purge: {
			knowledge: "Toxicity Down", knowledgeSuffix: '+',
			data: {
				[Items.Rarity.Common]: {points: -30, value: 20, suffixCount: 1},
				[Items.Rarity.Uncommon]: {points: -50, value: 40, suffixCount: 2},
				[Items.Rarity.Rare]: {points: -80, value: 80, suffixCount: 3},
				[Items.Rarity.Legendary]: {points: -120, value: 160,  suffixCount: 4},
			},
		},
	}));

	/** WILLPOWER */
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustCoreStatXP(CoreStat.Willpower, a), {
		BREAK_WILL_XP: {knowledge: "WillPower Down", knowledgeSuffix: '-', pointsFactor: -50, valueFactor: 0, data: templateXpPoints},
		BOLSTER_WILL_XP: {knowledge: "WillPower Up", knowledgeSuffix: '+', pointsFactor: 50, valueFactor: 75, data: templateXpPoints},
	}));
	/** NUTRITION  AND HUNGER */
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustCoreStat(CoreStat.Nutrition, a), {
		NUTRITION: {
			knowledge: "Nutrition Up", knowledgeSuffix: '+', pointsFactor: 5, valueFactor: 5,
			data: {
				weak: {points: 1, suffixCount: 0},
				common: {points: 2, suffixCount: 1},
				uncommon: {points: 4, suffixCount: 2},
				rare: {points: 10, suffixCount: 3},
				legendary: {points: 20, suffixCount: 4},
			},
		},
		HUNGER: {
			knowledge: "Nutrition Down", knowledgeSuffix: '-', pointsFactor: -1, valueFactor: 0,
			data: {
				weak: {points: 1, suffixCount: 0},
				common: {points: 2, suffixCount: 1},
				uncommon: {points: 4, suffixCount: 2},
				rare: {points: 10, suffixCount: 3},
				legendary: {points: 20, suffixCount: 4},
			},
		},
	}));

	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustCoreStatXP(CoreStat.Nutrition, a), {
		NUTRITION_XP: {
			knowledge: "Satiation Up", knowledgeSuffix: '+', pointsFactor: 2, valueFactor: 1,
			data: {
				weak: {points: 5, suffixCount: 0},
				common: {points: 10, suffixCount: 1},
				uncommon: {points: 20, suffixCount: 2},
				rare: {points: 50, suffixCount: 3},
				legendary: {points: 100, suffixCount: 4},
			},
		},
		HUNGER_XP: {
			knowledge: "Satiation Down", knowledgeSuffix: '-', pointsFactor: -2, valueFactor: 0,
			data: {
				weak: {points: 5, suffixCount: 0},
				common: {points: 10, suffixCount: 1},
				uncommon: {points: 20, suffixCount: 2},
				rare: {points: 50, suffixCount: 3},
				legendary: {points: 100, suffixCount: 4},
			},
		},
	}));

	/** Femininity **/
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustCoreStatXP(CoreStat.Femininity, a), {
		FEMININITY_DOWN_XP: {knowledge: "Femininity Down", knowledgeSuffix: '-', pointsFactor: -25, valueFactor: 10, data: templateXpPoints},
		FEMININITY_XP: {knowledge: "Femininity Up", knowledgeSuffix: '+', pointsFactor: 25, valueFactor: 10, data: templateXpPoints},
	}));

	/** HEALTH **/
	Object.append(effectLibrary, {
		HEAL_COMMON: {
			fun: (p) => {p.adjustCoreStat(CoreStat.Health, 20);},
			value: 20, knowledge: ["Health Up+"],
		},

		HEAL_UNCOMMON: {
			fun: (p) => {p.adjustCoreStat(CoreStat.Health, 40);},
			value: 50, knowledge: ["Health Up++"],
		},

		HEAL_RARE: {
			fun: (p) => {p.adjustCoreStat(CoreStat.Health, 75); p.adjustCoreStat(CoreStat.Toxicity, -50);},
			value: 150, knowledge: ["Health Up+++"],
		},

		HEAL_LEGENDARY: {
			fun: (p) => {p.adjustCoreStat(CoreStat.Health, 100); p.adjustCoreStat(CoreStat.Toxicity, -100);},
			value: 300, knowledge: ["Health Up++++"],
		},
	});

	/** FITNESS */
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustCoreStatXP(CoreStat.Fitness, a), {
		FITNESS_DOWN_XP: {knowledge: "Fitness Down", knowledgeSuffix: '-', pointsFactor: -25, valueFactor: 100, data: templateXpPoints},
		FITNESS_XP: {knowledge: "Fitness Up", knowledgeSuffix: '+', pointsFactor: 25, valueFactor: 100, data: templateXpPoints},
	}));

	/** HORMONES */
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustCoreStatXP(CoreStat.Hormones, a), {
		MALE_HORMONE_XP: {knowledge: "Male Hormones", knowledgeSuffix: '+', pointsFactor: -50, valueFactor: 25, data: templateXpPoints},
		FEMALE_HORMONE_XP: {knowledge: "Female Hormones", knowledgeSuffix: '+', pointsFactor: 50, valueFactor: 10, data: templateXpPoints},
	}));

	Object.append(effectLibrary, {
		HORMONAL_BALANCE_COMMON: {
			fun: (p) => {
				const h = p.stat(Stat.Core, CoreStat.Hormones);
				p.coreStats[CoreStat.Hormones] = h > 100 ? Math.min((h - 10), 100) : Math.max((h + 10), 100);
			},
			value: 500, knowledge: ["Hormonal Balance+"],
		},
	});

	/** PERVERSION **/
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustCoreStatXP(CoreStat.Perversion, a), {
		PERVERSION_DOWN_XP: {knowledge: "Perversion Down", knowledgeSuffix: '+', pointsFactor: -25, valueFactor: 10, data: templateXpPoints},
		PERVERSION_XP: {knowledge: "Perversion Up", knowledgeSuffix: '+', pointsFactor: 25, valueFactor: 10, data: templateXpPoints},
	}));
}
