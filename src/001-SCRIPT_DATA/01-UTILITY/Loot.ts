namespace App.Data {
	/**
	 * Filter creation shortcut
	 * @param test the test to apply to the object to
	 */
	function getFilter<C extends Items.Category>(test: (obj: Data.ItemTypeDescMap[C]) => boolean) {
		return (a: Items.ItemDescDictionary<Items.Category>, _b: unknown, _c: number) => {
			const o: Items.ItemDescDictionary<Items.Category> = {};
			for (const p in a)
				// @ts-expect-error FIXME this needs propagating the C type upstream
				if (test(a[p]))
					o[p] = a[p];
			return o;
		};
	}
	/**
	 * @param a Object property to test
	 * @param values array of values to check
	 */
	function any<C extends Items.Category, K extends keyof Data.ItemTypeDescMap[C]>(a: K, values: AlwaysArray<Data.ItemTypeDescMap[C][K]>) {
		const t = (o: Data.ItemTypeDescMap[C]) => {
			const ov = o[a];
			return Array.isArray(ov) ? ov.includesAny(...values) : values.includes(ov);
		};

		return getFilter(t);
	}

	export function makeLootTableItem<TCat extends Items.Category, K extends keyof Data.ItemTypeDescMap[TCat]>(
		cat: TCat, chance: number, maxCount: number, key?: K, values?: AlwaysArray<Data.ItemTypeDescMap[TCat][K]>,
		free?: boolean): Data.LootTableItem {
		const res: Data.LootTableItem = {
			type: cat,
			chance,
			maxCount,
		};
		if (key && values) {
			res.filter = any<TCat, K>(key, values)
		}
		if (free) {
			res.free = free;
		}
		return res;
	}

	export function makeLootTableItemNoFilter(type:  Items.Category | "coins", chance: number, min: number, max: number): Data.LootTableItem {
		return {type, chance, min, max};
	}
}
