declare namespace App {
	const enum Gender {
		It = -1,
		Female = 0,
		Male = 1,
		Shemale = 2
	}

	type DynamicValue<T> = T | ((player: Entity.Player) => T);

	type DynamicText = DynamicValue<string>;

	namespace VariableScope {
		interface Base<T extends string> {
			type: T;
		}

		type Player = Base<'player'>;
		type CurrentTask = Base<'current'>;
		interface Quest extends Base<'quest'> {
			id: string;
		}
		interface Job extends Base<'job'> {
			id: string;
		}

		type Defined = Player | Quest | Job;
		type Any = CurrentTask | Defined;

		export interface Scope {
			scope?: Any;
		}
	}
}
