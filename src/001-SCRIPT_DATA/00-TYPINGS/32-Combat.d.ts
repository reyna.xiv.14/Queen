declare namespace App.Data {
	export namespace Combat {
		export interface Enemy {
			name: string;
			title: string;
			health: number;
			maxHealth: number;
			energy: number;
			attack: number;
			defense: number;
			maxStamina: number;
			stamina: number;
			speed: number;
			moves: App.Combat.Style;
			gender: Gender;
			portraits?: string[];
			bust?: number;
			ass?: number;
		}

		type MoveUnlockTest = (player: App.Combat.Combatant) => boolean;

		export interface Move {
			description: string;
			icon: string;
			stamina: number;
			combo: number;
			speed: number;
			damage: number;
			unlock: MoveUnlockTest;
			miss: [string, string][];
			hit: [string, string][];
		}

		export interface StyleDefinition<Moves extends string> {
			moves: Record<Moves, Move>;
		}

		export namespace EncounterLoots {
			interface Base {
				chance: number;
				min: number;
				max: number;
			}

			interface Coins extends Base {
				type: 'Coins';
			}

			interface Random extends Base {
				type: 'Random';
			}

			interface Item extends Base {
				type: Items.Category;
				tag: string;
			}

			export type Any = Coins | Random | Item;
		}

		export interface EncounterDesc {
			enemies: string[],
			fatal: boolean;
			winPassage: string;
			losePassage: string;
			intro?: string;
			lootMessage?: string;
			loot?: EncounterLoots.Any[];
			winHandler?: (player: Entity.Player, encounterData: EncounterDesc) => void;
			loseHandler?: (player: Entity.Player, encounterData: EncounterDesc) => void;
			fleeHandler?: (player: Entity.Player, encounterData: EncounterDesc) => void;
		}

		export interface ClubEncounter {
			title: string;
			winsRequired: number;
			maxWins: number;
			encounter: string;
		}
	}
}
