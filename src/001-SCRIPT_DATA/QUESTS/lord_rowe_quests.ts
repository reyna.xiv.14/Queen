namespace App.Data {
	Object.append(quests, {
		DADDYSGIRL: {
			title: "Daddy's Little Girl",
			giver: "LordRowe",
			checks: [
				{type: "styleCategory", name: "DaddyGirl", value: 100, condition: "gte"},
				{type: "hairStyle", name: "pig tails", value: true},
				{type: "hairColor", name: "blond", value: true},
			],
			intro: "You walk up to NPC_NAME and give him a deep curtsy, taking special care to thrust out your pBUST cleavage to draw \
        his eye. He looks up at you apprehensively and for a moment you can catch the tiredness in his eyes. Clearly he has been \
        losing sleep over the disappearance of his daughter.\n\n\
        You introduce yourself and step forward. sp(Excuse me my Lord, but I've come to ask for a boon,) you state as NPC_NAME eyes \
        you up and down. It's clear from the look on his face that he has already surmised much about your current station in life.\n\n\
        s(And what would a 'lady' such as yourself need from me? Can't you see that I am lost in my grief for my precious missing \
        daughter? Begone harlot!) he exclaims, his hands motioning for the door.\n\n\
        That could have gone better. Perhaps there might be some other way to get his attention, such as preying on his obsession \
        with GF_NAME…",
			middle: "NPC_NAME says, s(I thought I told you to leave. Be gone and leave me to my grief!)",
			finish: "NPC_NAME looks up from his desk and takes you in. For a moment he seems puzzled and it's obvious that your superficial \
        resemblance to GF_NAME was playing on his mind.\n\n\
        s(Excuse me my dear, but why are you here? Did you get lost?) he asks. Without hesitating you flounce your way over and sit \
        on his lap, sp(Oooph!) you let out in an high voice, followed quickly by a girlish giggle.\n\n\
        sp(Sowwy… It wooks wike I wost my Daddy and I can't find him anywhere. Hey mistah, would you be my new Daddy?) you say, \
        while making a show of wiggling your butt on NPC_NAME's lap. It doesn't take long before you feel something hard pushing \
        back.\n\n\
        s(What a shame little one… I'm sure we can come to some sort of… agreement.) he replies, his hands gently encircling your \
        waist. You giggle again lightly and return his embrace.\n\n\
        It appears your attire, a perfect mix between perversity and coquettishness has captured his attention. Now what do you do?",
			journalEntry: "NPC_NAME seems caught up in his grief for his missing daughter. Perhaps if you were to alter your appearance in \
        a way that might draw out his 'fatherly' instincts, he might become more compliant?",
			journalComplete: "It seems that @@.npc;Lord Rowe's@@ obsession with GF_NAME had a rather 'abnormal' aspect to it. Once \
        you changed your appearance, he was more than happy to listen to you… so long as you sit on his lap like a good little girl. \
        In addition, you can gain access to his study at any hour of the day.",
		},

		LORD_ROWE_DELIVERY: {
			title: "Smuggler's Run: Special Delivery - Lord Rowe",
			giver: "LordRowe",
			pre: [{type: "quest", name: "LORD_ROWE_PORNO_DELIVERY", property: 'status', value: QuestStatus.Active, condition: "eq"}],
			checks: [{type: "item", name: "quest/lolita book", value: 1, condition: "gte"}],
			reward: [{type: Items.Category.LootBox, name: "uncommon lolita loot box", value: 1}],
			intro: "",
			middle: "You need to bring the following items to NPC_NAME:",
			finish: "NPC_NAME looks behind you to make sure no one is watching and then greedily takes the contraband novel from your hands. \
        He then looks you dead in the eye and says s(You were never here.)\n\n\
        So much for a thank-you, you suppose. And to think this dirty bastard was eying his own daughter like this… Before that \
        thought can get much further, NPC_NAME shoves something into your hand and bids you good day. It's clear that he wants you \
        to leave, and you don't have to guess why."
			,
			// Don't display in journal.
			journalEntry: "HIDDEN",
			journalComplete: "HIDDEN",
		},
	});
}
