namespace App.Data {
	Object.append(quests, {
		GAME_WON: {
			title: "virtual quest to track game won conditions",
			hidden: true,
			variables: {MONEY: false},
			intro: "",
			middle: "",
			finish: "",
			journalComplete: "HIDDEN",
			journalEntry: "HIDDEN",
		},
		GFWARDROBE: {
			title: "virtual quest for accessing the girlfriend wardrobe",
			hidden: true,
			intro: "",
			middle: "",
			finish: "",
			journalComplete: "HIDDEN",
			journalEntry: "HIDDEN",
		},
		FUTACOLLAR: {
			title: "virtual quest for finding the ancient futa collar",
			hidden: true,
			intro: "",
			middle: "",
			finish: "",
			journalComplete: "HIDDEN",
			journalEntry: "HIDDEN",
		},
		BERTIE_QUEEN_PT2_INFO: {
			title: "virtual quest to track various info sources",
			hidden: true,
			intro: "",
			middle: "",
			finish: "",
			journalComplete: "HIDDEN",
			journalEntry: "HIDDEN",
		},
	});
}
