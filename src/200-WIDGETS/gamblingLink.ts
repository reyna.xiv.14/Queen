Macro.add("gamblingLink", {
	handler() {
		const gameName = `Play ${this.args[0] as string}`;
		const link = this.args[1] as string;
		const player = setup.world.pc;
		const energy = player.stat(App.Stat.Core, App.CoreStat.Energy);
		if (energy < 1 || setup.world.phase >= App.DayPhase.LateNight) {
			App.UI.appendNewElement('span', this.output, `[${gameName}]`, ['state-disabled']);
		} else {
			this.output.append(App.UI.passageLink(gameName, link, () => {
				player.adjustCoreStat(App.CoreStat.Energy, -1);
				setup.world.nextPhase(1);
				if (!tags().includes('custom-menu')) {
					variables().gameBookmark = passage();
				}
			}));
		}
	},
});
