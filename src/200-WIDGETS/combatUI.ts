Macro.add("combatUI", {
	handler() {
		const appendDiv = (parent: ParentNode, id: string, classList?: string[]) => {
			const res = App.UI.appendNewElement('div', parent);
			if (classList) {
				res.classList.add(...classList);
			}
			res.id = id;
			return res;
		};

		const appendNewStatCell = (parent: HTMLDivElement, label: string, classList?: string[]) => {
			const cell = parent.appendNewElement('div', undefined, ['combat-stat-cell']);
			cell.appendNewElement('div', label + ':', ['combat-stat-name']);
			return cell.appendNewElement('div', undefined, classList);
		};

		setup.combat.drawUI();
		const combatUI = appendDiv(this.output, "CombatGUI", ['CombatGUI']);
		const combatPanel = appendDiv(combatUI, "combat-panel")
		appendDiv(combatPanel, "EnemyGUI", ["EnemyGUI"]);
		appendDiv(combatUI, "InitiativeBar");
		const combatStatsContainer = appendDiv(combatPanel, "PlayerCombatStatContainer", ["combat-stats-container"]);
		appendNewStatCell(combatStatsContainer, "Combat Style").appendNewElement('select').id = "combatStyles";
		appendNewStatCell(combatStatsContainer, "Stamina", ['combat-stat-meter'])
			.appendNewElement('div').id = "PlayerStaminaBar";
		appendNewStatCell(combatStatsContainer, "Combo", ['combat-stat-meter'])
			.appendNewElement('div').id = "PlayerComboBar";

		const cmdDiv = combatPanel.appendNewElement('div');
		cmdDiv.style.display = 'flex';
		cmdDiv.style.marginLeft = '5px';
		const commands = cmdDiv.appendNewElement('div');
		commands.appendNewElement('button', "Restore Stamina", ['combatButton']).id = "cmdRestoreStamina";
		commands.appendNewElement('br');
		commands.appendNewElement('button', "Defend", ['combatButton']).id = "cmdDefend";
		commands.appendNewElement('br');
		commands.appendNewElement('button', "Flee", ['combatButton']).id = "cmdFlee";

		appendDiv(cmdDiv, "CombatCommands");
	},
});

Macro.add("combatResults", {
	handler() {
		App.UI.appendFormattedText(this.output, {
			text: "Combar Results:",
			style: ["combat-results", "action-general"]
		});
		App.UI.appendNewElement('div', this.output).id = "WinDiv";
		setup.combat.drawResults();
	},
});
