Macro.add("coffinUI", {
	handler() {
		const appendDiv = (parent: ParentNode, id: string, classList?: string[]) => {
			const res = App.UI.appendNewElement('div', parent);
			if (classList) {
				res.classList.add(...classList);
			}
			res.id = id;
			return res;
		};

		const appendBetDiv = (parent: ParentNode, id: string, label: string) => {
			const res = App.UI.appendNewElement('div', parent);
			res.id = `CoffinBet${id}`;
			res.appendNewElement('div', `${label} Bet`).id = `betLabel${id}`;

			res.appendNewElement('div', "Him").id = `betLabel${id}b`;
			appendDiv(res, `betIcon${id}a`);
			res.appendNewElement('div', "You").id = `betLabel${id}c`;
			appendDiv(res, `betIcon${id}b`);
			appendDiv(parent, `CoffinBet${id}Blocker`);
		};

		const appendCofBox = (parent: ParentNode, id: string, label: string) => {
			const res = App.UI.appendNewElement('div', parent);
			res.id = `cofBoxWrapper${id}`;
			for (let i = 0; i < 8; ++i) {
				const d = appendDiv(res, `${label}box${i}`, ['"coffinNumBox"']);
				d.textContent = (i + 2).toString();
			}
			return res;
		};

		const div = appendDiv(this.output, 'GamblingGUI', ['GamblingGUI']);

		const left = appendDiv(div, "CoffinContainerLeft", ['CoffinContainerLeft']);
		const dice = appendDiv(left, "CoffinDiceContainer");
		appendDiv(dice, "RoundContainer");
		appendDiv(dice, "GamesPlayedContainer");
		const bet = appendDiv(left, "CoffinBetContainer");

		const pirateSel = appendDiv(bet, "coffinPirateSelect");
		const buttonCmdPreviousBet = pirateSel.appendNewElement('button', undefined, ["coffinRoundButton"]);
		buttonCmdPreviousBet.id = "cmdPrevBet";
		buttonCmdPreviousBet.innerHTML = '&#8249';
		pirateSel.appendNewElement('span').id = "CoffinNPCName";
		const buttonCmdNextBet = pirateSel.appendNewElement('button', undefined, ["coffinRoundButton"]);
		buttonCmdNextBet.id = "cmdNextBet";
		buttonCmdNextBet.innerHTML = '&#8250';

		appendBetDiv(bet, "1", "Top");
		appendBetDiv(bet, "2", "Bottom");

		const right = appendDiv(div, "CoffinContainerRight", ['CoffinContainerRight']);
		const coffinImg = right.appendNewElement('img');
		coffinImg.id = "coffinBoard";
		coffinImg.dataset.passage = "img_coffin";
		coffinImg.src = Story.get('img_coffin').text;
		appendCofBox(div, "1", "pc");
		appendCofBox(div, "2", "npc");

		appendDiv(div, "cofPlayerName").textContent = "You";
		appendDiv(div, "cofNPCName").textContent = "Opponent";

		const tray = appendDiv(div, "cofUItray");
		tray.appendFormattedText(
			"Welcome @@color:cyan;Shut the Coffin@@! The game is easy to play - roll the dice and if you hit an empty number, \
			mark it and roll again. The first player to mark five nails is the winner! If the board is filled until the \
			very last spot, then you enter <span style='color:orange'>SUDDEN DEATH</span> and the first person to hit a double \
			on the dice will win!\n\
			\n\
			You can play even if you don't have money by literally betting your ass. Just be careful about losing too much or \
			you might be a vegetable after your opponents are done with you!"
		);

		const appendBetButtons = (parent: ParentNode, groupId: string, buttons: [string, string][]) => {
			const group = App.UI.appendNewElement('div', parent, undefined, ["cofUIButtons"]);
			group.id = groupId;
			for (const d of buttons) {
				const button = group.appendNewElement('button', d[1], ["cofButton"]);
				button.id = d[0];
			}
		}

		appendBetButtons(div, "startButtons", [["cmdStart", "START GAME"], ["cmdQuit", "WALK AWAY"]]);
		appendBetButtons(div, "betButtons", [["cmdTakeBet", "TAKE BET"], ["cmdQuitBet", "WALK AWAY"]]);
		appendBetButtons(div, "playButtons", [["cmdRollDice", "ROLL DICE"]]);
		appendBetButtons(div, "endButtons", [["cmdPlayAgain", "PLAY AGAIN"], ["cmdQuitPlay", "WALK AWAY"]]);
	},
});
