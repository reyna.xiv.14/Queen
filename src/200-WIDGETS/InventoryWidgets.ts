namespace App.UI.Widgets {
	export namespace Inventory {
		class MessageViewer {
			#element: HTMLElement;
			constructor(element: HTMLElement) {
				this.#element = element;
			}

			showMessage(m: string) {
				replace(this.#element, m);
			}

			get element() {
				return this.#element;
			}
		}

		//#region inventory tabs
		abstract class InventoryOrClothingTab extends App.UI.Widgets.Tab {
			#tableElement: HTMLTableElement;
			#tableBody!: HTMLTableSectionElement;
			#logger: MessageViewer;

			constructor(name: string, longName: string) {
				super(name, name.capitalizeFirstLetter(), `Inventory - ${longName}`);
				this.#tableElement = document.createElement('table');
				this.#tableElement.classList.add('inventory-table');
				this.#logger = new MessageViewer(document.createElement('div'));
			}

			render() {
				const res = new DocumentFragment();
				const theader = appendNewElement('thead', this.#tableElement);
				const dummyRow = appendNewElement('tr', theader);
				const dummyCell = appendNewElement('th', dummyRow);
				dummyCell.colSpan = 999;
				dummyCell.style.fontWeight = "normal";
				dummyCell.append(this.#logger.element);
				// res.append(this.#logger.element);
				this.#tableBody = appendNewElement('tbody', this.#tableElement);
				res.append(this.#tableElement);
				this.update();
				return res;
			}

			public update() {
				replace(this.#tableBody, this.content());
			}

			protected get logger() {
				return this.#logger;
			}

			protected abstract content(): Node;

			protected static addCell(row: HTMLTableRowElement): HTMLTableCellElement {
				const res = document.createElement('td');
				row.append(res);
				return res;
			}

			protected static addButtonCell(row: HTMLTableRowElement, caption: string, handler: () => void): void {
				const cell = InventoryOrClothingTab.addCell(row);
				const button = document.createElement('button');
				button.classList.add('inventory');
				button.textContent = caption;
				button.addEventListener('click', handler);
				cell.append(button);
			}

			protected static makeHeaderRow(cellWidths: string[]): HTMLTableRowElement {
				const res = document.createElement('tr');
				res.classList.add('section-header');
				for (const w of cellWidths) {
					const cell = document.createElement('td');
					cell.style.width = w;
					res.append(cell);
				}
				return res;
			}

			protected addExamineButtonCell(row: HTMLTableRowElement, item: Items.InventoryItem | Items.IEquipmentItem): void {
				InventoryOrClothingTab.addButtonCell(row, "Examine", this._examineItem.bind(this, item));
			}

			private _examineItem(item: Items.InventoryItem | Items.IEquipmentItem) {
				this.logger.showMessage(item.examine(setup.world.pc, false));
			}
		}

		abstract class InventoryTab extends InventoryOrClothingTab {
			#inventory: Entity.InventoryManager;

			constructor(name: string, longName: string, inventory: Entity.InventoryManager) {
				super(name, longName);
				this.#inventory = inventory;
			}

			protected get inventory() {
				return this.#inventory;
			}

			protected addUseButtonCell(row: HTMLTableRowElement, item: Items.Consumable): void {
				InventoryOrClothingTab.addButtonCell(row, "Use", this._useItem.bind(this, item));
			}

			private _useItem(item: Items.Consumable) {
				this.logger.showMessage(setup.world.pc.useItem(item.id));
				Passages.StoryCaption.updateGameScore();
				this.update();
			}
		}

		abstract class TossableInventoryTab extends InventoryTab {
			#types: Data.ItemTypeInventory[];

			constructor(name: string, types: Data.ItemTypeInventory[], longName: string, inventory: Entity.InventoryManager) {
				super(name, longName, inventory);
				this.#types = types;
			}

			protected listItems(): Items.InventoryItem[] {
				return setup.world.pc.getItemByTypes(this.#types, true);
			}

			protected addTossButton(row: HTMLTableRowElement, item: Items.Consumable): void {
				InventoryTab.addButtonCell(row, "Toss", this._throwItem.bind(this, item));
			}

			private _throwItem(item: Items.Consumable): void {
				this.logger.showMessage(`You threw away ${item.description}.`);
				item.removeCharges(1);
				this.update();
			}
		}

		class ConsumableTab extends TossableInventoryTab {
			#favoritesEnabled: boolean;

			constructor(name: string, types: Data.ItemTypeConsumable[], longName: string, inventory: Entity.InventoryManager) {
				super(name, types, longName, inventory);
				this.#favoritesEnabled = Quest.isCompleted(setup.world.pc, "BETTER_LOCKER");
			}

			protected content() {
				const res = new DocumentFragment();
				const cellWidth: string[] = [];
				if (this.#favoritesEnabled) {
					cellWidth.push("1em");
				}
				// counts, toss, desc, use, examine
				cellWidth.push("4em", "5em", "100%", "5em", "5em");
				res.append(InventoryTab.makeHeaderRow(cellWidth));
				for (const item of this.listItems() as Items.Consumable[]) {
					const row = appendNewElement('tr', res);
					// favorite button
					if (this.#favoritesEnabled) {
						const favoriteCell = InventoryTab.addCell(row);
						favoriteCell.append(this._favoriteItemButton(item))
					}
					// count
					const countCell = InventoryTab.addCell(row);
					countCell.style.textAlign = "right";
					countCell.textContent = String(item.charges);
					// throw button
					super.addTossButton(row, item);
					// desc
					const desc = InventoryTab.addCell(row);
					$(desc).wiki(PR.printItem(item, setup.world.pc));

					super.addUseButtonCell(row, item);
					super.addExamineButtonCell(row, item);
				}
				return res;
			}

			private _favoriteItemButton(item: Items.Consumable) {
				const res = document.createElement('a');
				res.innerHTML = PR.getItemFavoriteIcon(item.isFavorite);
				res.addEventListener('click', () => this._favoriteItem(item));
				return res;
			}

			private _favoriteItem(item: Items.Consumable) {
				this.logger.showMessage(`You placed ${item.description} ${item.toggleFavorite() ? "in a special box" : "back to the common locker space"}.`);
				this.update();
			}
		}

		class MiscLootTab extends TossableInventoryTab {
			#enableToss: boolean;
			constructor(name: string, types: Data.ItemTypeInventory[], longName: string, inventory: Entity.InventoryManager, enableToss = false) {
				super(name, types, longName, inventory);
				this.#enableToss = enableToss;
			}

			override content(): Node {
				const res = new DocumentFragment();
				const cellWidth: string[] = [];
				cellWidth.push("4em"); // counts
				if (this.#enableToss) {
					cellWidth.push("5em");
				}
				cellWidth.push("100%", "5em", "5em"); // desc, empty or use, examine
				res.append(InventoryTab.makeHeaderRow(cellWidth));
				for (const item of super.listItems() as Exclude<Items.InventoryItem, Items.Reel>[]) {
					const row = appendNewElement('tr', res);
					// count
					const countCell = InventoryTab.addCell(row);
					countCell.style.textAlign = "right";
					countCell.textContent = String(item.charges);
					if (this.#enableToss && item instanceof Items.Consumable) {
						super.addTossButton(row, item);
					}
					// desc
					const desc = InventoryTab.addCell(row);
					$(desc).wiki(PR.printItem(item, setup.world.pc));
					if (item instanceof Items.Consumable) {
						super.addUseButtonCell(row, item);
					} else {
						// empty
						appendNewElement('td', row);
					}
					super.addExamineButtonCell(row, item);
				}
				return res;
			}
		}

		class GearTab extends InventoryOrClothingTab {
			#human: Entity.Human;

			constructor(human: Entity.Human) {
				super("gear", "Clothing and Weapons");
				this.#human = human;
			}

			content(): Node {
				const res = new DocumentFragment();
				res.append(InventoryOrClothingTab.makeHeaderRow(["9em", "100%", "5em"]));
				const eq = this.#human.equipment;
				for (const slot of Object.values(ClothingSlot)) {
					const items: Items.IEquipmentItem[] = [];
					const eqInSlot = eq[slot];
					if (eqInSlot) {
						items.push(eqInSlot);
					}
					items.push(...this.#human.wardrobeItemsBySlot(slot));
					items.sort(Items.compareByRank);
					if (items.length > 0) {
						let slotPrinted = false;
						for (const item of items) {
							const row = UI.appendNewElement('tr', res);
							const slotCell = InventoryTab.addCell(row);
							if (!slotPrinted) {
								slotCell.classList.add('inventory-slot-name');
								slotCell.textContent = slot;
								slotPrinted = true;
							}
							this._renderItem(row, item, item === eqInSlot);
						}
					}
				}
				return res;
			}

			private _renderItem(row: HTMLTableRowElement, item: Items.IEquipmentItem, isEquipped: boolean) {
				const descCell = InventoryTab.addCell(row);
				if (isEquipped) {
					GearTab._addWornItemPrefix(descCell, item);
				}
				$(descCell).wiki(PR.printItem(item, setup.world.pc));
				this.addExamineButtonCell(row, item);
			}

			private static _addWornItemPrefix(cell: HTMLTableCellElement, item: Items.IEquipmentItem) {
				cell.append('(');
				if (item.isLocked) {
					appendNewElement('span', cell, "locked", ['state-negative']);
				} else {
					appendNewElement('span', cell, "worn", ['inventory-item-worn']);
				}
				cell.append(') ');
			}
		}

		export class InventoryTabs extends App.UI.Widgets.TabWidget {
			constructor(human: Entity.Human) {
				super();
				this.addTab(new ConsumableTab("food", [Data.ItemTypeConsumable.Food], "Food and Drink", human.inventory));
				this.addTab(new ConsumableTab("potions", [Data.ItemTypeConsumable.Potion], "Potions and Drugs", human.inventory));
				this.addTab(new MiscLootTab("loot", [
					Data.ItemTypeSpecial.Quest,
					Data.ItemTypeConsumable.LootBox,
					Data.ItemTypeConsumable.MiscLoot,
				], "Loot Items", human.inventory));
				this.addTab(new MiscLootTab("misc",
					[
						Data.ItemTypeConsumable.MiscConsumable,
						Data.CosmeticsType.HairTool,
						Data.CosmeticsType.HairTreatment,
						Data.CosmeticsType.BasicMakeup,
						Data.CosmeticsType.ExpensiveMakeup,
					], "Miscellaneous Items", human.inventory, true));
				this.addTab(new GearTab(human));
				this.selectTabByIndex(0);
			}
		}

		export class CollarSwapTable extends InventoryOrClothingTab {
			constructor() {
				super("collar", "Collar Swap");
			}

			override content(): Node {
				const res = new DocumentFragment();
				const items = setup.world.pc.wardrobeItemsBySlot(ClothingSlot.Neck);
				const eqItem = setup.world.pc.equipment.neck;
				items.sort(Items.compareByRank);
				res.append(InventoryTab.makeHeaderRow(['5em', '100%', '5em']));
				if (eqItem) {
					const wornRow = document.createElement('tr');
					const wornCell = InventoryTab.addCell(wornRow);
					appendNewElement('span', wornCell, "Worn", ['inventory-item-worn']);
					const descCell = InventoryTab.addCell(wornRow);
					$(descCell).wiki(PR.printItem(eqItem, setup.world.pc));
					this.addExamineButtonCell(wornRow, eqItem);
					res.append(wornRow);
				}
				for (const item of items) {
					const row = document.createElement('tr');
					res.append(row);
					InventoryTab.addButtonCell(row, "Swap", this._swapCollar.bind(this, item));
					const descCell = InventoryTab.addCell(row);
					$(descCell).wiki(PR.printItem(item, setup.world.pc));
					this.addExamineButtonCell(row, item);
				}
				return res;
			}

			private _swapCollar(collar: Items.IEquipmentItem): void {
				const current = setup.world.pc.equipmentInSlot(ClothingSlot.Neck);
				const currentIsLocked = current ? current.isLocked : false;
				setup.world.pc.spendMoney(300, GameState.CommercialActivity.Jobs);
				setup.world.pc.wear(collar, currentIsLocked);
				this.update();
				// update avatar if shown
				if (settings.displayAvatar) {
					setup.avatar.drawPortrait();
				}
			}
		}
		// #endregion
	}
}

Macro.add("Inventory", {
	skipArgs: true,
	handler() {
		this.output.append(new App.UI.Widgets.Inventory.InventoryTabs(setup.world.pc).element);
	},
});

Macro.add("DrawCollarTable", {
	skipArgs: true,
	handler() {
		this.output.append(new App.UI.Widgets.Inventory.CollarSwapTable().render());
	},
});
