namespace App.UI {
	function questDialog(quest: App.Quest, npcId: string): Node {
		const res = new DocumentFragment();
		const ctx = quest.makeActionContext(setup.world);
		const qStatus = quest.status(ctx.world.pc);
		const scenes = quest.playScenes(ctx);
		for (const s of scenes) {
			res.append(s.text);
			App.UI.appendNewElement('br', res);
		}

		const applySceneActions = () => {
			for (const s of scenes) {
				for (const a of s.actions) {
					a.apply(ctx);
				}
			}
		}

		const buttonCssClasses = ['cmd-button'];

		if (qStatus === App.QuestStatus.Available) {
			const acceptButton = App.UI.appendNewElement('button', res, "Accept", buttonCssClasses);
			acceptButton.addEventListener('click', () => {
				applySceneActions();
				App.UI.replace("#QuestUI", questList(npcId));
			});
			res.append('  ');
			const declineButton = App.UI.appendNewElement('button', res, "Decline", buttonCssClasses);
			declineButton.addEventListener('click', () => {
				App.UI.replace("#QuestUI", questList(npcId));
			});
		} else if (qStatus === App.QuestStatus.Active) {
			const okayButton = App.UI.appendNewElement('button', res, "Okay, fine", buttonCssClasses);
			okayButton.addEventListener('click', () => {
				App.UI.replace("#QuestUI", questList(npcId));
			});
		} else { // "cancomplete"
			const allActions: App.Actions.Base[] = [];
			for (const s of scenes) {
				for (const a of s.actions) {
					allActions.push(a);
				}
			}
			if (allActions.length > 0) {
				const rewardsDiv = App.UI.makeElement('div');
				rewardsDiv.style.margin = '1ex';
				App.UI.appendNewElement('span', rewardsDiv, "Quest Rewards", ['task-cancomplete']);
				const rewardsRenderer = new App.Actions.TaskRewardRenderer(ctx, allActions, rewardsDiv);
				if (!rewardsRenderer.isEmpty) {
					res.append(rewardsDiv);
				}
			}
			const completeButton = App.UI.appendNewElement('button', res, "Complete Quest", ['cmd-button']);
			completeButton.addEventListener('click', () => {
				applySceneActions();
				if (App.Quest.byId("GAME_WON").isCompleted(setup.world.pc)) {
					Engine.play("GameWon");
				}
				Engine.play(variables().gameBookmark);
			});
		}

		return res
	}

	function questList(npcId: string): Node {
		const res = new DocumentFragment();
		const npc = setup.world.npc(npcId);
		const ql = App.Quest.list("any", setup.world.pc, npcId);
		$(res).wiki(`You approach ${npc.pName} to ask if there are any 'special tasks' that need to be done.`);
		for (const q of ql) {
			const qDiv = App.UI.appendNewElement('div', res);
			const qState = q.status(setup.world.pc);
			if (qState === App.QuestStatus.Completed) {
				App.UI.appendNewElement('span', qDiv, q.title(), ['state-disabled']);
				App.UI.appendNewElement('span', qDiv, " (COMPLETED)", ['state-neutral']);
			} else {
				const questLink = App.UI.appendNewElement('a', qDiv, q.title());
				questLink.addEventListener('click', () => {
					App.UI.replace("#QuestUI", questDialog(q, npcId));
				});
				if (qState === App.QuestStatus.Active) {
					App.UI.appendNewElement('span', qDiv, " (IN PROGRESS)", ['item-time']);
				} else if (qState === App.QuestStatus.CanComplete) {
					App.UI.appendNewElement('span', qDiv, " (IN PROGRESS)", ['task-cancomplete']);
				}
			}
		}
		$(res).wiki(App.UI.pInteractLinkStrip());
		return res;
	}

	Macro.add("questList", {
		handler() {
			this.output.append(questList(variables().menuAction as string));
		},
	});
}
