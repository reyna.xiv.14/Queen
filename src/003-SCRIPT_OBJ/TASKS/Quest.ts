namespace App {
	enum QuestStdAttribute { // property names begin with '_' to avoid clashes with task variables
		AcceptedOn = "_acceptedOn",
		CompletedOn = "_completedOn",
	}

	interface QuestStdAttributeTypes {
		[QuestStdAttribute.AcceptedOn]: number;
		[QuestStdAttribute.CompletedOn]: number;
	}

	function stdAttribute<A extends QuestStdAttribute>(
		state: GameState.TasksState | undefined, attribute: A, defaultValue: QuestStdAttributeTypes[A]
	): QuestStdAttributeTypes[A] {
		return state?.[attribute] as QuestStdAttributeTypes[A] ?? defaultValue;
	}

	function setStdAttribute<A extends QuestStdAttribute>(state: GameState.TasksState, attribute: A, value: QuestStdAttributeTypes[A]) {
		state[attribute] = value;
	}

	// ---------------------  Quests -------------------------
	export class Quest extends Task {
		static readonly #QUESTS: Map<string, Quest> = new Map();

		static loadQuests() {
			Quest.#QUESTS.clear();
			for (const id of Object.keys(Data.quests)) {
				Quest.#QUESTS.set(id, new Quest(id));
			}
		}

		/**
		 * Returns a list of quest entries that fit the criteria on the "flag" option.
		 * * cancomplete - Player can possibly complete these now.
		 * * available - Player can accept these quests.
		 * * completed - Quests the player has completed.
		 * * active - Quests that are currently active.
		 * * any - return all quests that match the above criteria.
		 * @param state
		 * @param player
		 * @param npc - The NPC ID of the quest giver. Optional.
		 */
		static list(state: QuestStatus| QuestStatus[] | "any", player: Entity.Player, npc?: string): Quest[] {
			let quests = [...Quest.#QUESTS.values()];
			if (npc) {
				quests = quests.filter(q => q.giverId === npc);
			}

			if (state === "any") { // TODO clarify "any" and "unavailable" relation
				return quests.filter(q => !q.isAtStatus(player, QuestStatus.Unavailable));
			}

			const states = Array.isArray(state) ? state : [state];

			return quests.filter(q => states.includes(q.status(player)));
		}

		/**
		 * Create quest by tag
		 */
		static byId(id: string): Quest {
			const res = Quest.#QUESTS.get(id);
			if (!res) {
				throw new Error(`No quest with id '${id}' exist`);
			}
			return res;
		}

		/**
		 * Creates a quest object that operates a virtual quest
		 *
		 * The virtual quest does not have a definition (in Data.Quests)
		 * and its only property is the quest ID. Thus it can be used only for
		 * testing or setting quest flags.
		 */
		static virtualById(id: string): Quest {
			return Quest.byId(id);
		}

		/**
		 * Handles time-dependent changes for active quests and quest rewards
		 */
		static nextDay(player: Entity.Player): void {
			for (const q of Quest.list([QuestStatus.Active, QuestStatus.Completed], player)) q.nextDay(player);
		}

		isActive(player: Entity.Player): boolean {
			const state = this.state(player);
			return stdAttribute(state, QuestStdAttribute.AcceptedOn, 0) > 0 &&
				stdAttribute(state, QuestStdAttribute.CompletedOn, 0) <= 0;
		}

		isCompleted(player: Entity.Player): boolean {
			return stdAttribute(this.state(player), QuestStdAttribute.CompletedOn, 0) > 0;
		}

		static isActive(player: Entity.Player, id: string): boolean {
			return Quest.byId(id).isActive(player);
		}

		static isCompleted(player: Entity.Player, id: string): boolean {
			return Quest.byId(id).isCompleted(player);
		}

		static allMissingItems(player: Entity.Player): Record<Data.ItemNameTemplateAny, number> {
			return Quest.list(QuestStatus.Active, player)
				.map(q => q.missingItems(player))
				.reduce((accumulator: Record<Data.ItemNameTemplateAny, number>, qr) => {
					for (const entry of Object.entries(qr)) {
						const ex = accumulator[entry[0]];
						accumulator[entry[0]] = ex ? ex + entry[1] : entry[1];
					}
					return accumulator;
				}, {});
		}

		static allRequiredItems(player: Entity.Player): Record<Data.ItemNameTemplateAny, number> {
			return Quest.list(QuestStatus.Active, player)
				.map(q => q.requiredItems())
				.reduce((accumulator: Record<Data.ItemNameTemplateAny, number>, qr) => {
					for (const entry of Object.entries(qr)) {
						const ex = accumulator[entry[0]];
						accumulator[entry[0]] = ex ? ex + entry[1] : entry[1];
					}
					return accumulator;
				}, {});
		}

		private constructor(id: string) {
			const data = Data.quests[id];
			// FIXME remplace virtual quests by a sane solution
			super(id, data, data.giver ? undefined : Entity.NpcId.Dummy);
		}

		override get taskData(): Data.Tasks.Quest {
			return super.taskData as Data.Tasks.Quest;
		}

		override get giver(): Entity.NPC {
			const res = super.giver;
			assertIsDefined(res);
			return res;
		}

		get journalEntry(): string {return this.taskData.journalEntry;}
		get journalCompleteEntry(): string {return this.taskData.journalComplete;}
		get checks(): ReadonlyArray<Data.Conditions.Any> | undefined {return this.taskData.checks;}

		/**
		 * Maps scene
		 */
		private _makeSceneData(sceneId: QuestStage): Data.Tasks.Scene {
			const res: Data.Tasks.Scene = {
				id: sceneId,
				text: this.taskData[sceneId],
			};
			const addToPost = (item: Data.Actions.Any) => {
				if (res.post) {
					res.post.push(clone(item))
				} else {
					res.post = [clone(item)];
				}
			};

			switch (sceneId) {
				case QuestStage.Intro:
					// eslint-disable-next-line unicorn/no-array-for-each
					this.taskData.onAccept?.forEach(addToPost);
					break;
				case QuestStage.Middle:
					break;
				case QuestStage.Finish:
					if (this.taskData.post) {
						res.post = clone(this.taskData.post);
					}
					if (this.taskData.reward) {
						// eslint-disable-next-line unicorn/no-array-for-each
						this.taskData.reward.forEach(addToPost);
					}

					const isConsumableItem = (r: Data.Conditions.Any): r is Data.Conditions.Item => {
						return r.type === 'item' &&
						!r.name.startsWith(`${Items.Category.Clothes}/`);
					};

					if (this.taskData.checks) {
						const itemsToConsume = this.taskData.checks.filter(isConsumableItem);
						for (const itemRule of itemsToConsume) {
							addToPost({type: "item", value: 0 - itemRule.value, name: itemRule.name});
						}
					}
					break;
			}
			return res;
		}

		protected override availableScenes(player: Entity.Player): Scene[] {
			const scenes: Scene[] = [];
			if (this.isAvailable(player)) {
				scenes.push(new QuestIntroScene(player, this._makeSceneData(QuestStage.Intro), this));
			} else if (this.canComplete(player)) {
				scenes.push(new QuestFinishingScene(player, this._makeSceneData(QuestStage.Finish), this));
			} else if (this.isActive(player)) {
				scenes.push(new QuestMiddleScene(player, this._makeSceneData(QuestStage.Middle), this));
			}
			return scenes;
		}

		// eslint-disable-next-line class-methods-use-this
		protected override getState(player: Entity.Player): Partial<Record<string, GameState.TasksState>> {
			return player.taskState(TaskType.Quest);
		}

		isAvailable(player: Entity.Player): boolean {
			if (this.isCompleted(player) || this.isActive(player)) return false;
			return Task.checkRequirements(this.taskData.pre ?? [], this.definitionContext);
		}

		canComplete(player: Entity.Player): boolean {
			if (!this.isActive(player)) return false;
			return Task.checkRequirements(this.taskData.checks ?? [], this.definitionContext);
		}

		status(player: Entity.Player): QuestStatus {
			const state = this.state(player);

			if (stdAttribute(state, QuestStdAttribute.CompletedOn, 0) > 0) return QuestStatus.Completed;

			if (stdAttribute(state, QuestStdAttribute.AcceptedOn, 0) <= 0) { // available or unavailabel
				return (Task.checkRequirements(this.taskData.pre ?? [], this.definitionContext)) ?
					QuestStatus.Available : QuestStatus.Unavailable;
			}
			return Task.checkRequirements(this.taskData.checks ?? [], this.definitionContext) ? QuestStatus.CanComplete : QuestStatus.Active;
		}

		isAtStatus(player: Entity.Player, status: QuestStatus): boolean {
			const state = this.state(player);
			switch (status) {
				case QuestStatus.Unavailable:
					return !state && !Task.checkRequirements(this.taskData.pre ?? [], this.definitionContext);
				case QuestStatus.Available:
					return !state && Task.checkRequirements(this.taskData.pre ?? [], this.definitionContext);
				case QuestStatus.Active:
					return !!state &&
						stdAttribute(state, QuestStdAttribute.AcceptedOn, 0) > 0 &&
						!Task.checkRequirements(this.taskData.checks ?? [], this.definitionContext);
				case QuestStatus.CanComplete:
					return !!state &&
						stdAttribute(state, QuestStdAttribute.AcceptedOn, 0) > 0 &&
						Task.checkRequirements(this.taskData.checks ?? [], this.definitionContext);
				case QuestStatus.Completed:
					return !!state && stdAttribute(state, QuestStdAttribute.CompletedOn, 0) > 0;
			}
		}

		missingItems(player: Entity.Player): Record<Data.ItemNameTemplateAny, number> {
			const res: Record<Data.ItemNameTemplateAny, number> = {};
			for (const c of this.checks ?? []) {
				switch (c.type) {
					case "item":
						if (c.hidden) {
							continue;
						}
						const invItem = player.getItemById(c.name);
						if (invItem === undefined) {
							res[c.name] = c.value;
						} else if (invItem instanceof Items.Consumable && invItem.charges < c.value) {
							res[c.name] = c.value - invItem.charges;
						}
						break;
				}
			}
			return res;
		}

		requiredItems(): Record<Data.ItemNameTemplateAny, number> {
			const res: Record<Data.ItemNameTemplateAny, number> = {};
			for (const c of this.checks ?? []) {
				switch (c.type) {
					case "item":
						if (c.hidden) {
							continue;
						}
						res[c.name] = c.value;
						break;
				}
			}
			return res;
		}

		/**
		 * Accepts the quest
		 *
		 * This method is not meant to be used for "Accept" button in the quest dialog because
		 * it creates its own copy of the INTRO scene.
		 */
		accept(world?: Entity.World): void {
			const scene = new QuestIntroScene(world?.pc ?? setup.world.pc, this._makeSceneData(QuestStage.Intro), this);
			this._calculateAndApplyScene(scene, world);
		}

		/**
		 * Completes the quest
		 */
		complete(world?: Entity.World): void {
			const scene = new QuestFinishingScene(world?.pc ?? setup.world.pc, this._makeSceneData(QuestStage.Finish), this);
			this._calculateAndApplyScene(scene, world);
		}

		/**
		 * Cancels the quest completely, removing its whole state as it was never taken
		 */
		cancel(player: Entity.Player): void {
			this.removeState(player);
		}

		/**
		 * Marks the quest as active without processing its intro scene
		 */
		markAsActive(player: Entity.Player): void {
			setStdAttribute(this.state(player, true), QuestStdAttribute.AcceptedOn, setup.world.day);
		}

		/**
		 * Marks quest as completed without processing the finishing scene
		 */
		markAsCompleted(player: Entity.Player): void {
			setStdAttribute(this.state(player, true), QuestStdAttribute.CompletedOn, setup.world.day);
		}

		/**
		 * Handles time-dependent changes for active quest
		 */
		nextDay(player: Entity.Player): void {
			const td = this.taskData;
			td.activeEffect?.call(this, player);
			if (td.completedEffect && this.isCompleted(player)) {
				td.completedEffect?.call(this, player);
			}
		}

		/**
		 * Day when the quest was accepted by player
		 */
		acceptedOn(player: Entity.Player, defaultValue = 0): number {
			return stdAttribute(this.state(player), QuestStdAttribute.AcceptedOn, defaultValue);
		}

		/**
		 * Day when the quest was completed by player
		 */
		completedOn(player: Entity.Player, defaultValue = 0): number {
			return stdAttribute(this.state(player), QuestStdAttribute.CompletedOn, defaultValue);
		}

		private _calculateAndApplyScene(scene: Scene, world?: Entity.World) {
			const ctx = this.makeActionContext(world);
			// eslint-disable-next-line unicorn/no-array-for-each
			scene.calculate(ctx, false)?.actions.forEach(a => a.apply(ctx));
		}
	}

	class QuestScene extends Scene {
		protected readonly quest: Quest;
		private readonly _sceneName: QuestStage;

		constructor(player: Entity.Player, sceneData: Data.Tasks.Scene,
			quest: Quest, sceneName: QuestStage) {
			super(quest, player, sceneData);

			this.quest = quest;
			this._sceneName = sceneName;
		}

		override get id() {
			return this._sceneName;
		}
	}

	class QuestIntroScene extends QuestScene {
		constructor(player: Entity.Player, sceneData: Data.Tasks.Scene, quest: Quest) {
			super(player, sceneData, quest, QuestStage.Intro);
		}

		/**
		 * Accept the quest and process triggers.
		 */
		override calculate(ctx: Actions.EvaluationContext): CalculatedScene {
			const res = super.calculate(ctx);
			if (!res) {
				throw `Quest intro scene calculated into null`;
			}

			res.actions.push(new Actions.SilentFunction((ctx) => {
				this.quest.markAsActive(ctx.player);
			}));
			return res;
		}
	}

	class QuestMiddleScene extends QuestScene {
		constructor(player: Entity.Player, sceneData: Data.Tasks.Scene, quest: Quest) {
			super(player, sceneData, quest, QuestStage.Middle);
		}

		/**
		 * Process all the rules in a scene and store the results in the object.
		 */
		 override calculate(ctx: Actions.EvaluationContext): CalculatedScene {
			const res = super.calculate(ctx);
			if (!res) {
				throw `Quest middle scene calculated into null`;
			}

			UI.appendNewElement('br', res.text);
			UI.appendNewElement('br', res.text);
			if (this.quest.checks) {
				const reqs = Task.evaluateRequirements(this.quest.checks, this.task.definitionContext, ctx.conditionContext);
				const renderVisitor = new Conditions.RequirementRenderer(ctx.conditionContext, res.text);
				reqs.root.acceptVisitor(renderVisitor);
			}
			return res;
		}
	}

	class QuestFinishingScene extends QuestScene {
		constructor(player: Entity.Player, sceneData: Data.Tasks.Scene, quest: Quest) {
			super(player, sceneData, quest, QuestStage.Finish);
		}

		/**
		 * Completes the quest.
		 */
		 override calculate(ctx: Actions.EvaluationContext): CalculatedScene {
			const res = super.calculate(ctx);
			if (!res) {
				throw `Quest finishing scene calculated into null`;
			}

			res.actions.push(new Actions.SilentFunction((ctx) => {
				this.quest.markAsCompleted(ctx.player);
			}));
			return res;
		}
	}
}
