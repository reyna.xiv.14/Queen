namespace App.Rogue {
	export interface EntityVisual {
		ch: string | null;
		fg: string | null;
		bg: string | null;
	}

	/** Base Entity Class - dig sites, traps, stairs, etc. */
	export class Entity {
		#xy: XY = new XY(0, 0);
		#level!: Level;
		#type: string | null = null;
		readonly #name: string;
		#encounter: string| null = null;
		#visual: EntityVisual;

		constructor(name: string, visual: EntityVisual) {
			this.#name = name;
			this.#visual = visual;
		}

		get type() {return this.#type;}
		set type(t) {this.#type = t;}
		get visual() {return this.#visual;}
		set visual(v) {this.#visual = v;}
		get xy() {return this.#xy;}
		set xy(xy) {this.#xy = xy;}
		get level() {return this.#level;}
		set level(l) {this.#level = l;}
		get name() {return this.#name;}
		get encounter() {return this.#encounter;}
		set encounter(e) {this.#encounter = e;}

		setPosition(xy: XY, level: Level) {
			this.#xy = xy;
			this.#level = level;
			return this;
		}
	}

	// Living creatures
	export class Being extends Entity implements ROT.SpeedActor {
		#speed: number;
		#hp: number;
		#awake: boolean;

		constructor(name: string, visual: EntityVisual) {
			super(name, visual);
			this.#speed = 100;
			this.#hp = 10;
			this.#awake = false;

			Engine.instance.scheduler.add(this, true); // Add newly created creatures to scheduler
		}

		get speed() {return this.#speed;}
		set speed(s) {this.#speed = s;}
		get awake() {return this.#awake;}
		set awake(s) {
			if (s && !this.#awake) {
				Engine.instance.textBuffer.write(`${this.name} notices you!`);
			}
			this.#awake = s;
		}

		// Used by ROT engine.
		getSpeed(): number {
			return this.#speed;
		}

		damage(d: number): void {
			this.#hp -= d;
			if (this.#hp <= 0) {
				this.die();
			}
		}

		die() {
			Engine.instance.scheduler.remove(this);
			this.level.removeBeing(this);
		}

		act() {
			console.log("RogueBeing: act() called");
			// Check if I need to wakeup.
			this.awake = this.xy.dist(App.Rogue.Engine.instance.player.xy) <= 10;

			if (this.awake) { // Chase the player.
				let x = Engine.instance.player.xy.x;
				let y = Engine.instance.player.xy.y;
				const passableCallback = (x: number, y: number) => {
					const k = encodeXy(x, y);
					return (Engine.instance.level.getBeings().get(k) === this)
						|| (Engine.instance.level.freeCells.has(k))
						&& (!(Engine.instance.level.getBeings().get(k) instanceof Rogue.Being)); // obstacle free
				}
				const astar = new ROT.Path.AStar(x, y, passableCallback, {topology: 4});

				const path: number[][] = [];
				const pathCallback = (x: number, y: number) => {
					path.push([x, y]);
				}

				astar.compute(this.xy.x, this.xy.y, pathCallback);
				// Now move.
				path.shift(); // remove mobs position
				if (path.length === 1) {
					Engine.instance.scheduler.remove(this);
					this.level.removeBeing(this);
					Engine.instance.engine.lock();

					setup.combat.initializeScene({flee: 30, fleePassage: "CombatAbamondGenericFlee"});
					setup.combat.loadEncounter(this.encounter as string);
					SugarCube.Engine.play("Combat");
					// Switch to combat mode here.
				} else if (path.length > 0) {
					x = path[0][0];
					y = path[0][1];
					const newXY = new Rogue.XY(x, y);
					this.level.setEntity(this, newXY);
				}
			}
		}
	}

	// The livingist creature - the player
	export class Player extends Being {
		#lightLevel: number;
		#lightDuration: number;
		readonly #keys: Record<number, number>;
		readonly #akeys: Record<number, number>;

		constructor(visual: EntityVisual) {
			super("pc", visual);

			this.#lightLevel = 1;
			this.#lightDuration = 0;

			this.#keys = {};
			this.#akeys = {};
			// Directions

			// North
			this.#keys[ROT.KEYS.VK_NUMPAD8] = 0;
			this.#keys[ROT.KEYS.VK_UP] = 0; // Arrow Key
			this.#akeys[ROT.KEYS.VK_W] = 0;
			// North East
			this.#keys[ROT.KEYS.VK_NUMPAD9] = 1;
			this.#keys[ROT.KEYS.VK_PAGE_UP] = 1;
			this.#akeys[ROT.KEYS.VK_E] = 1;
			// East
			this.#keys[ROT.KEYS.VK_NUMPAD6] = 2;
			this.#keys[ROT.KEYS.VK_RIGHT] = 2; // Arrow Key
			this.#akeys[ROT.KEYS.VK_D] = 2;
			// South East
			this.#keys[ROT.KEYS.VK_NUMPAD3] = 3;
			this.#keys[ROT.KEYS.VK_PAGE_DOWN] = 1;
			this.#akeys[ROT.KEYS.VK_X] = 3;
			// South
			this.#keys[ROT.KEYS.VK_NUMPAD2] = 4;
			this.#keys[ROT.KEYS.VK_DOWN] = 4; // Arrow Key
			this.#akeys[ROT.KEYS.VK_S] = 4;
			// South West
			this.#keys[ROT.KEYS.VK_NUMPAD1] = 5;
			this.#keys[ROT.KEYS.VK_END] = 1;
			this.#akeys[ROT.KEYS.VK_Z] = 5;
			// West
			this.#keys[ROT.KEYS.VK_NUMPAD4] = 6;
			this.#keys[ROT.KEYS.VK_LEFT] = 6; // Arrow Key
			this.#akeys[ROT.KEYS.VK_A] = 6;
			// North West
			this.#keys[ROT.KEYS.VK_NUMPAD7] = 7;
			this.#keys[ROT.KEYS.VK_HOME] = 1;
			this.#akeys[ROT.KEYS.VK_Q] = 7;

			// Reserved
			this.#keys[ROT.KEYS.VK_PERIOD] = -1;
			this.#keys[ROT.KEYS.VK_CLEAR] = -1;

			// Ascend / Descend / Dig
			this.#keys[ROT.KEYS.VK_NUMPAD5] = -1;
			this.#akeys[ROT.KEYS.VK_R] = -1;

			// Use Torch
			this.#keys[ROT.KEYS.VK_DIVIDE] = -1;
			this.#keys[ROT.KEYS.VK_SLASH] = -1;
			this.#akeys[ROT.KEYS.VK_T] = -1;
		}

		get lightLevel() {return this.#lightLevel;}
		set lightLevel(l) {this.#lightLevel = l;}
		get lightDuration() {return this.#lightDuration;}
		set lightDuration(d) {this.#lightDuration = d;}

		static get torches(): number {
			return setup.world.pc.inventory.charges(Items.Category.MiscConsumable, "torch");
		}

		static get shovels(): number {
			return setup.world.pc.inventory.charges(Items.Category.MiscConsumable, "shovel");
		}

		override act(): void {
			console.log("Player:act() called");
			this._printEntityAtLocationMessage();
			Engine.instance.textBuffer.flush();
			Engine.instance.engine.lock();
			$(document).on("keydown", this.handleEvent.bind(this));
		}

		override die(): void {
			Engine.instance.scheduler.remove(this);
			Engine.instance.over();
		}

		private _printEntityAtLocationMessage() {
			if (!this.level.freeCells.hasOwnProperty(this.xy.toString())) return;
			const entity = this.level.freeCells.get(xyToNumber(this.xy));
			if (entity === undefined) return;

			const keyCmd = settings.alternateControlForRogue ? "'r'" : 'NUMPAD5';

			switch (entity.type) {
				case null:
					break;
				case 'stairs_up':
					if (this.level.depth > 1) {
						Engine.instance.textBuffer.write("Press " + keyCmd + " to ascend a level.");
					} else {
						Engine.instance.textBuffer.write("Press " + keyCmd + " to exit.");
					}
					break;
				case 'stairs_down':
					Engine.instance.textBuffer.write("Press " + keyCmd + " to descend a level.");
					break;
				case 'dig_spot':
					Engine.instance.textBuffer.write("Press " + keyCmd + " to dig here");
					break;
			}
		}

		handleEvent(event: JQuery.KeyDownEvent): void {
			const keyHandled = this._handleKey(event.keyCode);

			if (keyHandled) {
				$(document).off("keydown");
				Engine.instance.engine.unlock();
			}
		}

		private _handleKey(code: number) {
			console.debug(`code=${code}`);

			const keys = settings.alternateControlForRogue ? this.#akeys : this.#keys;

			if (code in keys) {
				// Engine.instance.textBuffer.clear();

				// Traverse staircase
				if ((code === ROT.KEYS.VK_NUMPAD5) || (code === ROT.KEYS.VK_R)) {
					// Handle Up / Down
					if (this.xy.is(this.level.entranceXY)) {
						if (setup.world.pc.stat(Stat.Core, CoreStat.Energy) < 1) {
							Engine.instance.textBuffer.write("You are too tired to climb up.");
							return 1;
						}
						setup.world.pc.adjustCoreStat(CoreStat.Energy, -1);
						PR.refreshTwineMeter(CoreStat.Energy);
						Engine.instance.ascend();
						return true;
					} else if (this.xy.is(this.level.exitXY)) {
						if (setup.world.pc.stat(Stat.Core, CoreStat.Energy) < 1) {
							Engine.instance.textBuffer.write("You are too tired to climb down.");
							return 1;
						}
						setup.world.pc.adjustCoreStat(CoreStat.Energy, -1);
						PR.refreshTwineMeter(CoreStat.Energy);
						Engine.instance.descend();
						return true;
					} else if (this.level.isTreasure(this.xy)) {
						if (Player.shovels > 0) {
							Engine.instance.textBuffer.write("You start digging…");
							this.level.digAt(this.xy);

							// Randomly draw down charge.
							if (this.level.depth > (200 * State.random())) {
								const shovel = setup.world.pc.getItemById("miscConsumable/shovel") as Items.Consumable;
								setup.world.pc.useItem(shovel.id);
								Engine.instance.textBuffer.write("Your shovel breaks!");
							}
							Engine.instance.refreshStatus();
							return true;
						} else {
							Engine.instance.textBuffer.write("Out of shovels!!");
							return true;
						}
					}
					return true;
				}

				// Use a light
				if (code === ROT.KEYS.VK_SLASH || code === ROT.KEYS.VK_DIVIDE || code === ROT.KEYS.VK_T) {
					if (Player.torches > 0) {
						const torch = setup.world.pc.getItemById("miscConsumable/torch") as Items.Consumable;
						setup.world.pc.useItem(torch.id); // draw down a charge
						Engine.instance.textBuffer.write("You light a torch.");
						this.lightLevel = 10;
						this.lightDuration = 100;
						Engine.instance.drawWithLight(this.xy);
						Engine.instance.refreshStatus();
						return true;
					} else {
						Engine.instance.textBuffer.write("You don't have any torches!");
					}
				}

				const direction = keys[code];
				if (direction === -1) { /* noop */
					/* FIXME show something? */
					return true;
				}

				const dir = ROT.DIRS[8][direction];
				const xy = this.xy.plus(new Rogue.XY(dir[0], dir[1]));

				const xyNum = xyToNumber(xy);
				// Collision detection…
				if (!this.level.getFreeCells().has(xyNum)) {
					return true;
				}

				const being = this.level.beings.get(xyNum); // get monster ob
				// Check for monster at location
				if (being?.encounter) {
					Engine.instance.scheduler.remove(being);
					this.level.removeBeing(being);
					Engine.instance.engine.lock();

					setup.combat.initializeScene({flee: 30, fleePassage: "CombatAbamondGenericFlee"});
					setup.combat.loadEncounter(being.encounter);
					this.level.setEntity(this, xy);
					Engine.instance.redraw(xy);
					SugarCube.Engine.play("Combat");
					return true;
				}

				if (this.lightDuration < 20 && this.lightDuration > 1 && Math.floor((State.random() * 3)) === 0) Engine.instance.textBuffer.write("Your torch is sputtering.");
				if (this.lightDuration === 1) Engine.instance.textBuffer.write("Your torch goes out!");
				if (this.lightDuration > 0) this.#lightDuration--;
				if (this.lightDuration < 1) {
					if (settings.alternateControlForRogue) {
						Engine.instance.textBuffer.write("It is dark. Press 't' to light a torch.");
					} else {
						Engine.instance.textBuffer.write("It is dark. Press '/' to light a torch.");
					}
					this.lightLevel = 1;
				}

				if (this.lightDuration > 0 && State.random() < 0.10) {
					// get approximate direction to exit
					const exit: XY = this.level.exitXY;
					const directionToExit = Math.atan2(xy.y - exit.y, exit.x - xy.x); // y axis it from top to bottom

					const directionString = (angle: number) => {
						const sector = angle / (Math.PI / 4);
						if (angle < -3 || sector > 3) return "west";
						if (sector > 1) return "north";
						if (sector < -1) return "south";
						return "east";
					};
					Engine.instance.textBuffer.write(`A light breeze from the ${directionString(directionToExit)} causes your torch to flicker.`);
				}

				this.level.setEntity(this, xy);
				Engine.instance.redraw(xy);
				return true;
			}

			return false; /* unknown key */
		}
	}
}
