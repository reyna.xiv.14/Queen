namespace App.Entity {
	export type ItemCallback = (charges: number, name: string, itemClass: keyof Items.InventoryItemTypeMap) => void;
	export type ItemPredicate = (charges: number, name: string, itemClass: keyof Items.InventoryItemTypeMap) => boolean;
	/**
	 * Provides convenient methods for inspecting an inventory objects (@see GameState.Human.inventory
	 * and below the Player class).
	 *
	 * Might be used by NPCs in a future version, thus is not part of the Player class.
	 *
	 * This object is not meant to be serialized by SugarCube
	 *
	 */
	export class InventoryManager {
		static readonly MAX_ITEM_CHARGES = 100;

		private readonly _characterId: string;
		private _items: Record<string, Items.InventoryItem>;

		protected get state(): GameState.Human {
			return setup.world.state.character[this._characterId];
		}

		constructor(characterId: string) {
			this._characterId = characterId;
			// create item objects for each record
			this._items = {};
			this.forEachItemRecord(undefined, undefined, (charges, tag, itemClass) => {
				this._ensureWrapObjectExists(itemClass, tag, charges);
			});
		}

		/**
		 * Looks through all classes for item with the given tag and returns the item class
		 * @param tag
		 * @param findAll return all matching items (the first one otherwise)
		 * @private
		 * TODO refactor the loop to exit early if findAll = false
		 */
		private _findItemClass(tag: string): Items.CategoryInventory;
		private _findItemClass(tag: string, findAll: false): Items.CategoryInventory;
		private _findItemClass(tag: string, findAll: true): Items.CategoryInventory[];
		private _findItemClass(tag: string, findAll = false): Items.CategoryInventory | Items.CategoryInventory[] | null {
			const res: Items.CategoryInventory[] = [];
			for (const entry of Object.entries(this.state.inventory)) {
				if (entry[1].hasOwnProperty(tag)) {
					res.push(entry[0]);
				}
			}
			if (findAll) {
				return res;
			}
			return res.length > 0 ? res[0] : null;
		}

		forEachItemRecord<Callback extends ItemCallback>(tag: string | undefined, itemClass: Items.CategoryInventory | undefined,
			func: Callback, thisObject?: ThisParameterType<Callback>): void {
			const types: Items.CategoryInventory[] = itemClass === undefined ? Object.keys(this.state.inventory) : [itemClass];
			types.forEach(function (this: InventoryManager, type) {
				const value = this.state.inventory[type];
				if (!value) return;
				if (tag === undefined) {
					for (const n in value) {
						if (!value.hasOwnProperty(n)) continue;
						func.call(thisObject, value[n], n, type);
					}
				} else {
					if (value.hasOwnProperty(tag)) {
						func.call(thisObject, value[tag], tag, type);
					}
				}
			}, this);
		}

		everyItemRecord<Predicate extends ItemPredicate>(predicate: Predicate, tag?: string, itemClass?: Items.CategoryInventory,
			thisObject?: ThisParameterType<Predicate>
		): void {
			const types: Items.CategoryInventory[] = itemClass === undefined ? Object.keys(this.state.inventory) : [itemClass];
			types.every(function (this: InventoryManager, type) {
				const val = this.state.inventory[type];
				if (!val) return false;
				if (tag !== undefined) {
					if (val.hasOwnProperty(tag)) {
						return predicate.call(thisObject, val[tag], tag, type);
					}
				} else {
					for (const n in val) {
						if (!val.hasOwnProperty(n)) continue;
						if (!predicate.call(thisObject, val[n], n, type)) return false;
					}
					return true;
				}
				return false;
			}, this);
		}

		private _ensureWrapObjectExists<
			T extends keyof Items.InventoryItemTypeMap
		>(itemClass: T, tag: string, _charges: number, id?: string): Items.InventoryItem {
			if (id === undefined) id = Items.makeId(itemClass, tag);
			if (!this._items.hasOwnProperty(id)) {
				// charges is stored already, hence '0' to prevent stack overflowing
				this._items[id] = Items.factory(itemClass, tag, this, 0);
			}
			return this._items[id];
		}

		filter(predicate: (item: Items.InventoryItem) => boolean, thisObject?: unknown): Items.InventoryItem[] {
			const res: Items.InventoryItem[] = [];
			this.forEachItemRecord(undefined, undefined, function (this: InventoryManager, ch, nm, cl) {
				const itemId = Items.makeId(cl, nm);
				const itemWrapObject = this._ensureWrapObjectExists(cl, nm, ch, itemId);
				if (predicate.call(thisObject, itemWrapObject)) res.push(itemWrapObject);
			}, this);
			return res;
		}

		charges(itemClass: Items.CategoryInventory | undefined, tag: string): number {
			let res = 0;
			this.forEachItemRecord(tag, itemClass, (n) => {res += n;});
			return res;
		}

		setCharges(itemClass: Items.CategoryInventory, tag: string, count: number): number {
			const cl = (itemClass === undefined) ? this._findItemClass(tag, true)[0] : itemClass;
			const clamped = Math.clamp(Math.floor(count), 0, InventoryManager.MAX_ITEM_CHARGES);
			const clValues = this.state.inventory[cl];
			if (clValues) {
				if (clamped === 0) {
					if (this.state.inventory.hasOwnProperty(cl) && clValues.hasOwnProperty(tag)) {
						delete clValues[tag];
						delete this._items[Items.makeId(cl, tag)];
					}
				} else {
					if (!this.state.inventory.hasOwnProperty(cl)) this.state.inventory[cl] = {};
					clValues[tag] = clamped;
					this._ensureWrapObjectExists(itemClass, tag, clamped);
				}
			}
			return clamped;
		}

		/**
		 * Adds (or removes) charges to a given item. If item is not in the inventory and Amount > 0, item record
		 * is created.
		 * If Amount < 0, resulting number of charges does not go below zero.
		  * @returns new number of charges
		 */
		addCharges(itemClass: Items.CategoryInventory, tag: string, amount: number): number {
			const cl = (itemClass === undefined) ? this._findItemClass(tag, true)[0] : itemClass;
			if (cl === undefined) throw new Error(`No item tagged '${tag}'`);
			// slight hack to not allow items that don't exist to be added to the inventory.
			const data = Items.tryGetItemsDictionary(cl);
			if (data == null) throw new Error(`No item class '${cl}' exists.`);
			if (!data.hasOwnProperty(tag)) throw new Error(`No item tagged '${tag}' exists in class '${cl}'`);
			if (!this.state.inventory.hasOwnProperty(cl)) this.state.inventory[cl] = {};

			const clInv = this.state.inventory[cl]!;
			if (!clInv.hasOwnProperty(tag)) {
				clInv[tag] = 0;
			}

			// Items with Charges in their data grant that many charges when added to the inventory.
			// All usage however only subtracts 1 charge.
			if (amount > 0) {
				amount = amount * Items.getCharges(cl, tag);
			} else {
				amount = amount === 0 ? Items.getCharges(cl, tag) : amount; // add charges if no charge specified?
			}

			return this.setCharges(cl, tag, clInv[tag] + amount);
		}

		isAtMaxCapacity(itemClass: Items.CategoryInventory, tag: string): boolean {
			return this.charges(itemClass, tag) >= InventoryManager.MAX_ITEM_CHARGES;
		}

		addItem<T extends keyof Items.InventoryItemTypeMap>(itemClass: T, tag: string, charges: number): Items.InventoryItemTypeMap[T] {
			type R = Items.InventoryItemTypeMap[T];
			this.addCharges(itemClass, tag, charges === undefined ? 1 : charges);
			return this._items[Items.makeId(itemClass, tag)] as R;
		}

		removeItem(id: Data.ItemNameTemplateInventory): void {
			const n = Items.splitId(id);
			this.setCharges(n.category, n.tag, 0);
		}

		/**
		 * Adds item to the set of favorites
		 */
		addFavorite(id: string): void {
			this.state.inventoryFavorites.add(id);
		}

		/**
		 * Removes item from the favorites set
		 */
		deleteFavorite(id: string): void {
			this.state.inventoryFavorites.delete(id);
		}

		/**
		 * Tests whether the item is in the favorites set
		 */
		isFavorite(id: string): boolean {
			return this.state.inventoryFavorites.has(id);
		}
	}

	export class PlayerInventoryManager extends InventoryManager {
		private readonly _reelInSlots: Record<number, Items.Reel | null>;

		constructor() {
			super('pc');
			this._reelInSlots = {};
			for (const slot in this.state.slots) {
				if (!this.state.slots.hasOwnProperty(slot)) continue;
				const reelTag = this.state.slots[slot];
				this._reelInSlots[slot] = reelTag == null ? null : Items.factory(Items.Category.Reel, reelTag, this)
			}
		}

		protected override get state(): GameState.Player {
			return super.state as GameState.Player;
		}

		/**
		* Attempt to pick a reel from inventory by Id and then equip it to a slot. It will remove any reel
		* equipped in that slot and place it back in the inventory.
		*/
		equipReel(toEquipID: Data.ItemNameTemplate<Items.Category.Reel>, reelSlot: number): void {
			const nm = Items.splitId(toEquipID);
			this.addCharges(Items.Category.Reel, nm.tag, -1);

			const reelTag = this.state.slots[reelSlot];
			if (reelTag != null) {
				this.addCharges(Items.Category.Reel, reelTag, 1);
			}

			this.state.slots[reelSlot] = nm.tag;
			this._reelInSlots[reelSlot] = Items.factory(Items.Category.Reel, nm.tag, this);
		}

		/**
		 * Remove an equipped reel and place it in the inventory.
		 */
		removeReel(slotID: number): void {
			const reelTag = this.state.slots[slotID];
			if (reelTag) {
				this.addCharges(Items.Category.Reel, reelTag, 1);
				this.state.slots[slotID] = null;
				this._reelInSlots[slotID] = null;
			}
		}

		/**
		* Turn the equipped reels into an array to iterate/read.
		*/
		equippedReelItems(): Items.Reel[] {
			const equippedReels = Object.values(this._reelInSlots).filter(o => !_.isNil(o));
			return (equippedReels === undefined) ? [] : equippedReels as Items.Reel[];
		}

		reelSlots(): Record<number, Items.Reel | null> {
			return this._reelInSlots;
		}
	}

	export class ClothingManager {
		private readonly _wardrobeItems: Array<Items.Clothing|Items.Weapon> = []; // TODO make it a Set
		private _equippedItems: Partial<Record<ClothingSlot, Items.Clothing | Items.Weapon | null>> = {};

		// eslint-disable-next-line class-methods-use-this
		private get _state(): GameState.Player {
			return setup.world.state.character.pc as GameState.Player;
		}

		private get _wardrobe() {
			return this._state.wardrobe;
		}

		private get _equipment() {
			return this._state.equipment;
		}

		/**
		 * Creates object for tracking equipment state
		 */
		static equipmentRecord(id: Data.ItemNameTemplateEquipment, isLocked = false): {id: Data.ItemNameTemplateEquipment; locked: boolean;} {
			return {id, locked: isLocked ?? false};
		}

		constructor() {
			for (const id of this._wardrobe) {
				// wardrobe lists item ids
				const t = Items.splitId(id);
				this._ensureWrapObjectExists(t.tag, id);
			}

			for (const [slot, eq] of Object.entries(this._equipment)) {
				if (eq === undefined) continue;
				if (eq === null) {this._equippedItems[slot] = null; continue;}
				const n = Items.splitId(eq.id);
				this._equippedItems[slot] = Items.factory(n.category, n.tag, this);
			}
		}

		private _ensureWrapObjectExists(tag: string, id?: string): Items.Clothing | Items.Weapon{
			if (id === undefined) id = Items.makeId(Items.Category.Clothes, tag);
			for (const item of this._wardrobeItems) {
				if (item.id === id) return item;
			}
			const res = Items.factory(Items.Category.Clothes, tag, this);
			this._wardrobeItems.push(res);
			return res;
		}

		/**
		 * Finds slot in which item with the given Id is worn
		 */
		private _findWornSlot(id: string): ClothingSlot | null {
			for (const [slot, eq] of Object.entries(this._equippedItems)) {
				if (eq?.id === id) return slot;
			}
			return null;
		}

		/**
		 * Returns true an item with the given Id is worn in the given slot. If slot param is omitted,
		 * every slot is checked.
		 */
		isWorn(id: string, slot?: ClothingSlot): boolean {
			if (slot === undefined) return this._findWornSlot(id) != null;
			return this._equipment[slot]?.id === id;
		}

		/**
		 * Is item in the named slot is locked?
		 * @returns True is there is an item in the slot and it's locked, false otherwise
		 */
		isLocked(slot: ClothingSlot): boolean {
			return this._equipment[slot]?.locked === true;
		}

		/**
		 * Useful helper method.
		 */
		setLock(slot: ClothingSlot, lock: boolean): void {
			const eq = this._equipment[slot];
			if (eq) {
				eq.locked = lock;
			}
		}

		/**
		 * Wear item and optionally set its 'locked' state.
		 * @param id
		 * @param lock lock or unlock item. Leaves locked state as is if the parameter is omitted.
		 */
		wear(id: string, lock?: boolean): void {
			let slot = this._findWornSlot(id);
			if (slot === null) { // currently the item is not worn
				for (let i = 0; i < this._wardrobeItems.length; ++i) {
					if (this._wardrobeItems[i].id !== id) continue;
					const itm = this._wardrobeItems[i];
					slot = itm.slot;

					const slotsToUndress: ClothingSlot[] = itm.restrict;
					slotsToUndress.push(slot);
					// handle restriction by removing items from the restricted slots
					for (const sl of slotsToUndress) {
						this.takeOffSlot(sl);
					}
					this._equippedItems[slot] = itm;
					this._wardrobeItems.splice(i, 1);
					this._equipment[slot] = ClothingManager.equipmentRecord(itm.id, itm.isLocked);
					this._wardrobe.splice(i, 1);
					break;
				}
			}
			if (slot != null && lock !== undefined) {
				this.setLock(slot, lock);
			}
		}

		takeOffSlot(slot: ClothingSlot): void {
			const itm = this._equippedItems[slot];
			if (itm) {
				delete this._equippedItems[slot];
				delete this._equipment[slot];
				this._wardrobeItems.push(itm);
				this._wardrobe.push(itm.id); // push(Id) ?
			}
		}

		takeOff(id: string): void {
			for (const [slot, itm] of Object.entries(this._equippedItems)) {
				if (itm && itm.id === id) {
					this.takeOffSlot(slot);
					break;
				}
			}
		}

		addItem(name: string, wear = false): Items.Clothing {
			const item = Items.factory(Items.Category.Clothes, name, this);
			this._wardrobe.push(item.id);
			this._wardrobeItems.push(item);
			if (wear) {
				this.wear(item.id);
			}
			return item;
		}

		get wardrobe(): Array<Items.Clothing|Items.Weapon> {
			return this._wardrobeItems;
		}

		get equipment(): Partial<Record<ClothingSlot, Items.Clothing | Items.Weapon | null>> {
			return this._equippedItems;
		}

		// #region Equipment sets
		get storedSets(): GameState.EquipmentSet[] {
			return this._state.equipmentSets ?? [];
		}

		saveCurrentSet(name: string): void {
			const items: Partial<Record<ClothingSlot, Data.ItemNameTemplateEquipment>> = {};
			for (const [s, eq] of Object.entries(this.equipment)) {
				if (eq && !eq.isLocked) {
					items[s] = eq.id;
				}
			}
			const sets = this.storedSets;
			sets.push({name, items});
			this._state.equipmentSets = sets;
		}

		deleteSet(indexOrName: number | string): void {
			if (typeof indexOrName === "number") {
				if (indexOrName >= 0 && indexOrName < this.storedSets.length) {
					this._state.equipmentSets.splice(indexOrName, 1);
				}
			} else {
				this._state.equipmentSets = this.storedSets.filter(set => set.name !== indexOrName);
			}
		}

		wearSet(indexOrName: number | string): void {
			if (typeof indexOrName === "number") {
				if (indexOrName >= 0 && indexOrName < this.storedSets.length) {
					const set = this.storedSets[indexOrName];
					for (const [s, id] of Object.entries(set.items)) {
						if (!this.isLocked(s) && !this.isWorn(id, s)) {
							this.wear(id);
						}
					}
					setup.avatar.drawPortrait();
				}
			} else {
				const index = this.storedSets.findIndex(s => s.name === indexOrName);
				if (index >= 0) {
					this.wearSet(index);
				}
			}
		}

		// #endregion
	}
}
