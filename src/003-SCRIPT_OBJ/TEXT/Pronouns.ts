namespace App.Text {
	export interface PronounDefinition {
		subject: string;
		object: string;
		possessive: {
			dependent: string;
			independent: string;
		};
		reflexive: string;
	}

	export class Pronouns {
		constructor(definition: PronounDefinition, noun?: string) {
			this.#def = definition;
			this.#noun = noun ?? definition.subject;
		}

		get subject(): string {return this.#def.subject;}
		get object(): string {return this.#def.object;}
		get possessivePronoun(): string {return this.#def.possessive.independent;}
		get possessive(): string {return this.#def.possessive.dependent;}
		get reflexive(): string {return this.#def.reflexive;}
		get noun(): string {return this.#noun;}

		/* eslint-disable @typescript-eslint/naming-convention */
		get Pronoun(): string {return _.capitalize(this.subject);}
		get PossessivePronoun(): string {return _.capitalize(this.possessivePronoun);}
		get Possessive(): string {return _.capitalize(this.possessive);}
		get Object(): string {return _.capitalize(this.object);}
		get Reflexive(): string {return _.capitalize(this.reflexive);}
		get Noun(): string {return _.capitalize(this.noun);}
		/* eslint-enable @typescript-eslint/naming-convention */

		get he(): string {return this.subject;}
		get him(): string {return this.object;}
		get his(): string {return this.possessive;}
		get himself(): string {return this.reflexive;}
		get boy(): string {return this.noun;}

		/* eslint-disable @typescript-eslint/naming-convention */
		get He(): string {return this.Pronoun;}
		get Him(): string {return this.Object;}
		get His(): string {return this.Possessive;}
		get Himself(): string {return this.Reflexive;}
		get Boy(): string {return this.Noun;}
		/* eslint-enable @typescript-eslint/naming-convention */

		get she(): string {return this.subject;}
		get her(): string {return this.object;}
		get hers(): string {return this.possessivePronoun;}
		get herself(): string {return this.reflexive;}
		get girl(): string {return this.noun;}
		get woman(): string {return this.noun === "girl" ? "woman" : "man";}
		get women(): string {return this.noun === "girl" ? "women" : "men";}

		/* eslint-disable @typescript-eslint/naming-convention */
		get She(): string {return this.Pronoun;}
		get Her(): string {return this.Object;}
		get Hers(): string {return this.PossessivePronoun;}
		get Herself(): string {return this.Reflexive;}
		get Girl(): string {return this.Noun;}
		get Woman(): string {return _.capitalize(this.woman);}
		get Women(): string {return _.capitalize(this.women);}
		/* eslint-enable @typescript-eslint/naming-convention */

		static get(gender: Gender, noun?: string): Pronouns {
			// TODO cache common cases when noun is undefined
			switch (gender) {
				case Gender.Female:
				case Gender.Shemale:
					return new Pronouns(Data.Pronouns.third.feminine, noun);
				case Gender.Male:
					return new Pronouns(Data.Pronouns.third.masculine, noun);
				case Gender.It:
					return new Pronouns(Data.Pronouns.third.neuter, noun);
			}
		}

		static get pc(): Pronouns {
			if (Pronouns.#pcPronouns == null) {
				Pronouns.#pcPronouns = new Pronouns(Data.Pronouns.second.singular);
			}
			return Pronouns.#pcPronouns;
		}

		readonly #def: PronounDefinition;
		readonly #noun: string;
		static #pcPronouns: Pronouns | null = null;
	}
}
