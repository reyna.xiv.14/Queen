Object.append(App.Data.Travel.scenes, {
	islaHarborGuardedGirlfriendRoom: {
		entry: "attempt",
		actors: {
			gf: App.Entity.CharacterId.Girlfriend,
			guard: 'npc:Jarvis',
		},
		fragments: {
			attempt: {
				type: "text",
				text: "As you attempt to gain entry into @{gf.name|@@'s Room@@}\
				the steward steps forward and blocks your way.\n\n\
				@{guard.name|(say)|, \"@@Excuse me, but I can't allow you to enter any further than this. \
				The young miss isn't around, but I dare to say she wouldn't like you rifling through her things.@@\"}\n\n\
				//Perhaps if there was some way to distract him…//",
			},
		},
	},
	islaHarborGuardedStudy: {
		entry: "attempt",
		actors: {
			gf: App.Entity.CharacterId.Girlfriend,
			guard: 'npc:Jarvis',
		},
		fragments: {
			attempt: {
				type: "text",
				text: "You move forward toward's the @@.location-name;Governor's Study@@, but the steward steps forward and blocks your way.\
				\
				@{guard.name|(say)|, \"@@Sorry miss, but the Governor isn't taking any visitors now. \
				He's quite distraught over the disappearance of his daughter. Please come back some other time… or don't.@@\"}\n\n\
				//How can you convince him to let you through without exposing your embarassing identity?//",
			},
		},
	},
});
