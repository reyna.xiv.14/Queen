namespace App.Data.Travel {
	const npcUticus = "npc:Uticus";
	Object.append(Travel.scenes, {
		highHillDistrictGuarded: {
			entry: "attempt",
			actors: {guard: "npc:Justus"},
			fragments: {
				attempt: {
					type: "text",
					text: `You move forward toward's the entrance to the @@.location-name;Hill Hill District@@, but the Guard Captain steps
				forward and blocks your way.

				@{guard|(say)|&, "@@There's no way I'll let a vagrant such as yourself pass through these gates. Be gone slut, ply your trade somewhere else!@@"}

				//Surely there can be some way to convince him to let you pass?//`,
				},
			},
		},
		goldenIsleGovernorMansionGuarded: {
			entry: "attempt",
			actors: {guard: npcUticus},
			fragments: {
				attempt: {
					type: "text",
					text: `As you approach the entrance to the @@.location-name;Governor's Mansion@@ a huge giant of a man stops you.

				@{guard|(say)|&, "@@Oi yee can't be entering there without my say so!@@"}

				You take one look at his giant frame and equally impressive cutlass and decide that proceeding would be a very unwise idea.`,
				},
			},
		},
		goldenIsleGovernorMansionJobNotDone: {
			entry: "attempt",
			actors: {guard: npcUticus},
			fragments: {
				attempt: {
					type: "text",
					text: `As you approach the entrance to the @@.location-name;Governor's Mansion@@, @{guard|(stop)} you.

				@{guard|"@@Hey @{pc}, if you want to be entering don't you think you have to take care of sumthin first?@@"}

				He makes a motion to his trousers and his barely contained monstrous cock.`,
				},
			},
		},
		goldenIsleGovernorMansionUticusUnsatisfied: {
			entry: "attempt",
			actors: {guard: npcUticus},
			fragments: {
				attempt: {
					type: "text",
					text: `As you approach the entrance to the @@.location-name;Governor's Mansion@@, @{guard|(stop)} you.

				@{guard|"@@Hey @{pc}, why don't you come back later and we'll have another round o' it? Maybe then I'll \
				be satisfied enough with your whore ass to let ya see the Boss.@@"}

				It seems that @{guard} won't be satisfied with just a fuck or two, you'll have to keep letting \
				him molest you with his monstrous black cock until he says he's had enough.`,
				},
			},
		},
	});
}
